%% Parametry wahadła

m = 0.2; % Masa wahadła
M = 0.5; % Masa wózka
I = 0.005426; % Bezwładność wahadła
L = 0.256; % Długość wahadła
l = 0.256;
f_p = 0.01; % Częstotliwość s
f_c = 0.01; % Częstotliwość c
% g = 9.81; % Przyśpieszenie ziemskie
b = 0.1; % Współczynnik tarcia wózka

% A = [0, ((M+m)*g)/(M*L), b/(M*L), 0;
%      1,  0, 0, 0;
%      0, -(m*g)/M, -b/M, 1;
%      0, 0, 1, 0];
% 
% B = [1/(M*L);
%      0;
%      1/M;
%      0];
% 
% C = [1 0 0 0;
%      0 0 1 0];
% D = [0;
%      0];
p = I*(M+m)+M*m*l^2; %denominator for the A and B matrices

A = [0      1              0           0;
     0 -(I+m*l^2)*b/p  (m^2*g*l^2)/p   0;
     0      0              0           1;
     0 -(m*l*b)/p       m*g*l*(M+m)/p  0];
B = [     0;
     (I+m*l^2)/p;
          0;
        m*l/p];
C = [1 0 0 0;
     0 0 1 0];
D = [0;
     0];


sim('Schemat_wahadla.slx');

%% Pend_model
% Te nastawy wydają się odpowiadać układowi
m = 0.14; % Masa wahadła
F_t = 0.768;
M = 0.5; % Masa wózka
I = 0.00282; % Bezwładność wahadła
L = 0.36693; % Długość wahadła
g = 9.81; % Przyśpieszenie ziemskie
b = 4; % Współczynnik tarcia wózka
b_c = 2;
b_p = 0.3;
cart_zad = 0;
theta_zad = 0;
distorption_delay = 0.5;
distorption_amplitude = pi/2;
time_sample = 0.01;
% sim('pend_model.slx');
flag = 0;

% linearized pendulum model
A = [0 1 0 0;
    ((M+m)*m*g*l)/(I*(M+m)+M*m*l^2) 0 0  (-m*b*l)/(I*(M+m)+M*m*l^2);
    0 0 0 1;
    (m^2*g*l^2)/(I*(M+m)+M*m*l^2) 0 0 (-(I+m*l^2)*b)/(I*(M+m)+M*m*l^2)];

B = [0;
    -m*l/(I*(M+m)+M*m*l^2);
    0;
    (I+m*l^2)/(I*(M+m)+M*m*l^2)];

C = [0 0 1 0];

D = 0;

