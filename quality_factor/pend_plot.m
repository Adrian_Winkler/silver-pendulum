% load('upPendulum')
% load sim_data
figure(1)
load('exp_data')

subplot(5,1,1)
plot((1:size(e(1).values(2000:3000),1))/100,e(1).values(2000:3000)), hold on
plot(simout.time, simout.signals.values(:,1))
title('Sterowanie');
xlabel('Czas [s]'),ylabel('Wypełnienie'), grid on
xlim([0 10])

subplot(5,1,2)
plot((1:size(e(2).values(2000:3000),1))/100, e(2).values(2000:3000)), hold on
plot(simout.time, simout.signals.values(:,2))
title('Przemieszczenie');
xlabel('Czas [s]'),ylabel('[m]'), grid on
xlim([0 10])


exp_pos_x = (1:size(e(2).values(2000:3000),1))/100;
exp_pos_y = e(2).values(2000:3000);

sim_pos_x = simout.time;
sim_pos_y = simout.signals.values(:,2);

subplot(5,1,3)
plot((1:size(e(3).values(2000:3000),1))/100, e(3).values(2000:3000)*10000), hold on
plot(simout.time, simout.signals.values(:,3))
title('Prędkość wózka');
xlabel('Czas [s]'),ylabel('[m/s]'), grid on
xlim([0 10])

subplot(5,1,4)
plot((1:size(e(4).values(2000:3000),1))/100, e(4).values(2000:3000)), hold on
plot(simout.time, simout.signals.values(:,4))
title('Kąt wahadła');
xlabel('Czas [s]'),ylabel('[rad]'), grid on
xlim([0 10])

exp_ang_x = (1:size(e(4).values(2000:3000),1))/100;
exp_ang_y = e(4).values(2000:3000);

sim_ang_x = simout.time;
sim_ang_y = simout.signals.values(:,4);

subplot(5,1,5)
plot((1:size(e(5).values(2000:3000),1))/100, e(5).values(2000:3000)), hold on
plot(simout.time, simout.signals.values(:,5))
title('Prędkość kątowa');
xlabel('Czas [s]'),ylabel('[rad/s]'), grid on
xlim([0 10])

rmse_pos = 0;
rmse_ang = 0;

rmse_pos_vec = zeros(1,1000);
rmse_ang_vec = zeros(1,1000);

rmse_pos_vec_perc = zeros(1,1000);
rmse_ang_vec_perc = zeros(1,1000);


for i = 10:1000
    rmse_pos = abs(exp_pos_y(i)-sim_pos_y(1000*i));
    rmse_pos_vec_perc(i) = abs((exp_pos_y(i)-sim_pos_y(1000*i))./exp_pos_y(i));
    rmse_pos_vec(i) = rmse_pos;
    rmse_ang = abs(exp_ang_y(i)-sim_ang_y(1000*i));
    exp_ang_y(i) = exp_ang_y(i)+10;
    sim_ang_y(1000*i) = sim_ang_y(1000*i) + 10;
	rmse_ang_vec_perc(i) = abs((exp_ang_y(i)-sim_ang_y(1000*i))./exp_ang_y(i).*10);

    rmse_ang_vec(i) = rmse_ang;
end

%%
figure(2);
plot(rmse_pos_vec);
figure(3);
plot(rmse_ang_vec_perc);


rmse_pos_max = max(rmse_pos_vec)
rmse_ang_max = max(rmse_ang_vec)

rmse_pos_mean = mean(rmse_pos_vec)
rmse_ang_mean = mean(rmse_ang_vec)

rmse_pos_mean_perc = sum(rmse_pos_vec_perc)/10
rmse_ang_mean_perc = sum(rmse_ang_vec_perc)/10

rmse_pos_median = median(rmse_pos_vec)
rmse_ang_median = median(rmse_ang_vec)




