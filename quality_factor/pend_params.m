clear 
close all

% Te nastawy wydają się odpowiadać układowi
m = 0.2; % Masa wahadła
F_m = 5;  
M = 0.6; % Masa wózka
L = 0.36693; % Długość wahadła
I = (1/3)*m*L.^2; %0.00282; % Bezwładność wahadła
g = 9.81; % Przyśpieszenie ziemskie
b_c = 4; % 2
b_p = 0.3; % 0.3
theta_int = 0;
pulse_amp = 1;
div = 1;
div_speed = 1;
pulse_phase_delay = 0.01;

%%
clear 
close all
% Te nastawy wydają się odpowiadać układowi
m = 0.2; % Masa wahadła
F_m = 5;  
M = 0.6; % Masa wózka
L = 0.36693; % Długość wahadła
I = 0.00282; % Bezwładność wahadła
g = 9.81; % Przyśpieszenie ziemskie
b_c = 2; % 2
b_p = 0.3; % 0.3
theta_int = 0;
pulse_amp = 1;
div = 1;
div_speed = 1;
pulse_phase_delay = 0.01;

figure('Renderer', 'painters', 'Position', [10 10 900 600])
load('pend_params');
sim('pend_model_2020');
save sim_data simout
pend_plot
saveas(gcf,'pulse_pend_plot','svg');
%%
clear 
close all
% Te nastawy wydają się odpowiadać układowi
m = 0.144; % Masa wahadła
F_m = 6;  
M = 1; % Masa wózka
L = 0.36693; % Długość wahadła
I = 0.005; % Bezwładność wahadła
g = 9.81; % Przyśpieszenie ziemskie
b_c = 3.7; % 2
b_p = 1; % 0.3
theta_int = 0;
pulse_amp = 1;
div = 0.24;
div_speed = 0.85;
pulse_phase_delay = 0;

figure('Renderer', 'painters', 'Position', [10 10 900 600])
load('pend_params');
sim('pend_model_2020');
save sim_data simout
pend_plot
saveas(gcf,'pulse_pend_plot','svg');
%%
clear 
close all
% Te nastawy wydają się odpowiadać układowi
m = 0.144; % Masa wahadła
F_m = 6;  
M = 1; % Masa wózka
L = 0.36693; % Długość wahadła
I = 0.005; % Bezwładność wahadła
g = 9.81; % Przyśpieszenie ziemskie
b_c = 3.7; % 2
b_p = 1; % 0.3
theta_int = 0;
pulse_amp = 1;
div = 0.24;
div_speed = 0.85;
pulse_phase_delay = 0;

figure('Renderer', 'painters', 'Position', [10 10 900 600])
load('pend_params');
sim('pend_model_2020');
save sim_data simout
pend_plot
saveas(gcf,'pulse_pend_plot','svg');


%% eksperymenty
clear 
close all
% Te nastawy wydają się odpowiadać układowi
m = 0.144; % Masa wahadła
F_m = 6;  
M = 1; % Masa wózka
L = 0.36693; % Długość wahadła
l = L;
I = 0.005; % Bezwładność wahadła
g = 9.81; % Przyśpieszenie ziemskie
b_c = 3.7; % 2
b_p = 1; % 0.3
theta_int = 0;
pulse_amp =  1;
div = 0.24;
div_speed = 0.85;
pulse_phase_delay = 0;

figure('Renderer', 'painters', 'Position', [10 10 900 600])

sim('pend_model_2020');
save sim_data simout
% pend_plot
pend_plot_copy

saveas(gcf,'pulse_pend_lin_nl_comp','svg');