

clear 
close all
load('pend_params');
sim('pend_model_2020');
save sim_data simout
figure(1)
subplot(5,1,1)
uppdata=load('upPendulum');
e = uppdata.P1_ExpData.signals;
load('sim_data')
plot((1:size(e(1).values(2000:end),1))/100,e(1).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,1))
title('Sterowanie');
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

subplot(5,1,2)
plot((1:size(e(2).values(2000:end),1))/100, e(2).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,2))
title('Przemieszczenie wózka');
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
subplot(5,1,3)
plot((1:size(e(3).values(2000:end),1))/100, e(3).values(2000:end)*10000), hold on
plot(simout.time, simout.signals.values(:,3))
title('Prędkość wózka');
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

subplot(5,1,4)
plot((1:size(e(4).values(2000:end),1))/100, e(4).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,4))
title('Kąt wahadła');
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

subplot(5,1,5)
plot((1:size(e(5).values(2000:end),1))/100, e(5).values(2000:end)), hold on
title('Prędkość kątowa');
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

saveas(gcf,'up_pendulum_plot','svg'); 