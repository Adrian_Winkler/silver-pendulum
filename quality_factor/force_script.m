figure(1)
dutyValue = [0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1];
force = [1 3.1 4.9 5.8 6.2 6.6 7 7.1 7.2];
plot(dutyValue, force,  'o', 'LineWidth', 4),grid on,hold on
title('Zależność generowanej siły od wypełnienia PWM')
x = linspace(0.131,1,100);
plot(x, 3.8444*log(x) + 7.8335,  '--', 'LineWidth', 1),grid on,hold on
xlabel('Wypełnienie PWM'),ylabel('Siła [N]')
saveas(gcf,'force_plot','svg'); 