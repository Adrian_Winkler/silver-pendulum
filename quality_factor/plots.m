% load('gain0_2_period10s_duty10perc_Pendulum.mat')
% Sygnał o wzmocnieniu 0.2 i okres 
% a = P1_ExpData.signals
% load('gain0_4_period10s_duty10perc_Pendulum.mat')
% b = P1_ExpData.signals
% load('gain0_6_period10s_duty3perc_Pendulum.mat')
% c = P1_ExpData.signals
% load('gain0_8_period10s_duty3perc_Pendulum.mat')
% d = P1_ExpData.signals
% load('gain1_period10s_duty3perc_Pendulum.mat')
% e = P1_ExpData.signals
% load('pend90_deg_Pendulum.mat')
% f = P1_ExpData.signals
% load('pend90deg_blocked_Pendulum.mat')
% g = P1_ExpData.signals
% load('pulsePendulum9_37_gain0_3_period_7s.mat')
% h = P1_ExpData.signals
% load('pulsePendulum9_40_gain1_period_7s.mat')
% i = P1_ExpData.signals
% load('pulsePendulum9_44_gain0_3_period_7s_reversed.mat')
% j = P1_ExpData.signals
% load('sinusPendulum.mat')
% k = P1_ExpData.signals
% load('sinusPendulum9_30_gain0_3_freq_0_5.mat')
% l = P1_ExpData.signals
% load('sinusPendulum9_30.mat')
% m = P1_ExpData.signals
% load('upPendulum.mat')
% n = P1_ExpData.signals
load('exp_data')
load('sim_data')

% figure(1)
% subplot(5,1,1)
% plot(a(1).values(2000:end)), hold on
% plot(simout.time, simout.signals.values(:,1))
% title('Wzmocnienie 0.2 Okres 10s Wypełnienie 10%');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(a(2).values(2000:end)), hold on
% plot(simout.signals.values(:,2))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(a(3).values(2000:end)), hold on
% plot(simout.Data(:,3))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(a(4).values(2000:end)), hold on
% plot(simout.Data(:,4))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(a(5).values(2000:end)), hold on
% plot(simout.Data(:,5))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

% figure(2)
% subplot(5,1,1)
% plot(b(1).values(2000:end))
% title('Wzmocnienie 0.4 Okres 10s Wypełnienie 10%');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(b(2).values(2000:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(b(3).values(2000:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(b(4).values(2000:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(b(5).values(2000:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

% figure(3)
% subplot(5,1,1)
% plot(c(1).values(2000:end))
% title('Wzmocnienie 0.6 Okres 10s Wypełnienie 3%');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(c(2).values(2000:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(c(3).values(2000:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(c(4).values(2000:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(c(5).values(2000:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on


% figure(4)
% subplot(5,1,1)
% plot(d(1).values(1000:end))
% title('Wzmocnienie 0.8 Okres 10s Wypełnienie 3%');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(d(2).values(1000:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(d(3).values(1000:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(d(4).values(1000:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(d(5).values(1000:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

figure(5)
subplot(5,1,1)
plot((1:size(e(1).values(2000:end),1))/100,e(1).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,1))
title('Wzmocnienie 1 Okres 10s Wypełnienie 3%');
legend('Sterowanie')
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
subplot(5,1,2)
plot((1:size(e(2).values(2000:end),1))/100, e(2).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,2)*0.9)
title('Przemieszczenie');
legend('Przemieszczenie wózka')
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
subplot(5,1,3)
plot((1:size(e(3).values(2000:end),1))/100, e(3).values(2000:end)*3000), hold on
plot(simout.time, simout.signals.values(:,3))
title('Prędkość');
legend('Prędkość wózka')
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
subplot(5,1,4)
plot((1:size(e(4).values(2000:end),1))/100, e(4).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,4))
title('Kąt');
legend('Kąt wahadła')
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
subplot(5,1,5)
plot((1:size(e(5).values(2000:end),1))/100, e(5).values(2000:end)), hold on
plot(simout.time, simout.signals.values(:,5))
title('Prędkość kątowa');
legend('Prędkość kątowa wahadła')
xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% 
% figure(6)
% subplot(5,1,1)
% plot(f(1).values(1:end))
% title('Wahadło wychylone 90 stopni od pionu');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(f(2).values(1:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(f(3).values(1:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(f(4).values(1:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(f(5).values(1:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% 
% figure(7)
% subplot(5,1,1)
% plot(g(1).values(1291:4140))
% title('Wahadło wychylone 90 stopni od pionu, wózek zablokowany');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(g(2).values(1291:4140))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(g(3).values(1291:4140))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(g(4).values(1291:4140))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(g(5).values(1291:4140))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% 
% figure(8)
% subplot(5,1,1)
% plot(h(1).values(2100:end))
% title('Wzmocnienie 0.3 Okres 7s Sterowanie prostokąt z lewej do prawej');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(h(2).values(2100:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(h(3).values(2100:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(h(4).values(2100:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(h(5).values(2100:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% 
% figure(9)
% subplot(5,1,1)
% plot(i(1).values(1400:end))
% title('Wzmocnienie 1 Okres 7s Sterowanie prostokąt');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(i(2).values(1400:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(i(3).values(1400:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(i(4).values(1400:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(i(5).values(1400:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% figure(10)
% subplot(5,1,1)
% plot(j(1).values(693:end))
% title('Wzmocnienie 0.3 Okres 7s Sterowanie prostokąt z prawej do lewej');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(j(2).values(693:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(j(3).values(693:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(j(4).values(693:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(j(5).values(693:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% 
% figure(11)
% subplot(5,1,1)
% plot(k(1).values(1925:end))
% title('Sinus');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(k(2).values(1925:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(k(3).values(1925:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(k(4).values(1925:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(k(5).values(1925:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% figure(12)
% subplot(5,1,1)
% plot(l(1).values(722:end))
% title('Wzmocnienie 0.3 Częstotliwość 0.5Hz Sterowanie sinus');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(l(2).values(722:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(l(3).values(722:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(l(4).values(722:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(l(5).values(722:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% figure(13)
% subplot(5,1,1)
% plot(m(1).values(724:end))
% title('Sinus');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(m(2).values(724:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(m(3).values(724:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(m(4).values(724:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(m(5).values(724:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% 
% figure(14)
% subplot(5,1,1)
% plot(n(1).values(837:end))
% title('Wahadło na górze');
% legend('Sterowanie')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,2)
% plot(n(2).values(837:end))
% title('Przemieszczenie');
% legend('Przemieszczenie wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,3)
% plot(n(3).values(1:end))
% title('Prędkość');
% legend('Prędkość wózka')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,4)
% plot(n(4).values(837:end))
% title('Kąt');
% legend('Kąt wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on
% subplot(5,1,5)
% plot(n(5).values(837:end))
% title('Prędkość kątowa');
% legend('Prędkość kątowa wahadła')
% xlabel('Czas [próbki]'),ylabel('Wartość'), grid on

