load('pliki_z_danymi/4_05/sr0800_2022/craneExpDataNoiseRemoved9_52.mat')
load('PIDqualitydata.mat')
thr = 6000;
figure(1)
plot(simout.time(thr:end)-132,simout.signals.values(thr:end,7))
figure(2)
plot(simoutPID.Time,simoutPID.Data(:,4))


xlqr = simout.time(thr:end)-132;
ylqr = simout.signals.values(thr:end,7);

xpid = simoutPID.Time;
ypid = simoutPID.Data(:,4);

elqr = ylqr.^2;
epid = ypid.^2;

ise_lqr = trapz(0.02, elqr);
ise_pid = trapz(0.02, epid);

% przeregulowanie |e1/e2|*100%
lqr_e1 = 0.12;
lqr_e2 = -0.0107379;


pid_e1 = 0.522064;
pid_e2 = -0.340626;

% wynik w procentach 
lqr_prze = abs(lqr_e1/lqr_e2)*100;
pid_prze = abs(pid_e1/pid_e2)*100;