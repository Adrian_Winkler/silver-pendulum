% ZNALEŹĆ DATE KIEDY DALIŚMY PID
% 4 kwietnia 2022
% regulator PID
% W symulacji


figure('Renderer', 'painters', 'Position', [10 10 900 600])
% figure(1)
% subplot(5,1,1)
% plot(1:size(n(1).values(1837:end),1),n(1).values(1837:end))
% title('Sterowanie');
% xlabel('Czas [s]'),ylabel('Wypełnienie'), grid on
% subplot(5,1,2)
% plot(1:size(n(1).values(1837:end),1),n(2).values(1837:end))
% title('Przemieszczenie');
% xlabel('Czas [s]'),ylabel('[m]'), grid on
% subplot(5,1,3)
% plot(1:size(n(1).values(1837:end),1),n(3).values(1837:end))
% title('Prędkość wózka');
% xlabel('Czas [s]'),ylabel('[m/s]'), grid on
% subplot(5,1,4)
% plot((1:size(n(1).values(1837:end),1))/100,n(4).values(1837:end))
title('Kąt wahadła');
xlabel('Czas [s]'),ylabel('[rad]'), grid on, hold on,
thr = 11000;
factor = 0.85;
% simoutPID.Data(thr:end,4)= simoutPID.Data(thr:end,4)*factor;
% simoutJaPierdole.Data(20000:150000,4)=simoutJaPierdole.Data(20000:150000,4)*1.1;
plot(simoutPID.Time,simoutPID.Data(:,4))
xlim([0 36])
% subplot(5,1,5)
% plot(1:size(n(1).values(1837:end),1),n(5).values(1837:end))
% title('Prędkość kątowa');
% xlabel('Czas [s]'),ylabel('[rad/s]'), grid on
saveas(gcf,'pid_plot','svg');
