/*
 * test4.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test4".
 *
 * Model version              : 1.185
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed May 04 10:16:47 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "test4.h"
#include "test4_private.h"
#include "test4_dt.h"

/* list of Simulink Desktop Real-Time timers */
const int RTWinTimerCount = 1;
const double RTWinTimers[2] = {
  0.002, 0.0,
};

/* Block signals (auto storage) */
B_test4_T test4_B;

/* Continuous states */
X_test4_T test4_X;

/* Block states (auto storage) */
DW_test4_T test4_DW;

/* Real-time model */
RT_MODEL_test4_T test4_M_;
RT_MODEL_test4_T *const test4_M = &test4_M_;
static void rate_scheduler(void);

/*
 *   This function updates active task flag for each subrate.
 * The function is called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (test4_M->Timing.TaskCounters.TID[2])++;
  if ((test4_M->Timing.TaskCounters.TID[2]) > 4) {/* Sample time: [0.01s, 0.0s] */
    test4_M->Timing.TaskCounters.TID[2] = 0;
  }

  test4_M->Timing.sampleHits[2] = (test4_M->Timing.TaskCounters.TID[2] == 0);
}

/*
 * This function updates continuous states using the ODE5 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE5_A[6] = {
    1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0
  };

  static const real_T rt_ODE5_B[6][6] = {
    { 1.0/5.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

    { 3.0/40.0, 9.0/40.0, 0.0, 0.0, 0.0, 0.0 },

    { 44.0/45.0, -56.0/15.0, 32.0/9.0, 0.0, 0.0, 0.0 },

    { 19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0.0, 0.0 },

    { 9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0,
      0.0 },

    { 35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE5_IntgData *id = (ODE5_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T *f4 = id->f[4];
  real_T *f5 = id->f[5];
  real_T hB[6];
  int_T i;
  int_T nXc = 4;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  test4_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE5_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[0]);
  rtsiSetdX(si, f1);
  test4_output();
  test4_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE5_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[1]);
  rtsiSetdX(si, f2);
  test4_output();
  test4_derivatives();

  /* f(:,4) = feval(odefile, t + hA(3), y + f*hB(:,3), args(:)(*)); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE5_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[2]);
  rtsiSetdX(si, f3);
  test4_output();
  test4_derivatives();

  /* f(:,5) = feval(odefile, t + hA(4), y + f*hB(:,4), args(:)(*)); */
  for (i = 0; i <= 3; i++) {
    hB[i] = h * rt_ODE5_B[3][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[3]);
  rtsiSetdX(si, f4);
  test4_output();
  test4_derivatives();

  /* f(:,6) = feval(odefile, t + hA(5), y + f*hB(:,5), args(:)(*)); */
  for (i = 0; i <= 4; i++) {
    hB[i] = h * rt_ODE5_B[4][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f5);
  test4_output();
  test4_derivatives();

  /* tnew = t + hA(6);
     ynew = y + f*hB(:,6); */
  for (i = 0; i <= 5; i++) {
    hB[i] = h * rt_ODE5_B[5][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4] + f5[i]*hB[5]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Model output function */
void test4_output(void)
{
  real_T *lastU;
  real_T rtb_Encoder500PPR[5];
  real_T rtb_ResetEncoders1;
  real_T rtb_Product_g;
  real_T rtb_TrigonometricFunction1;
  real_T rtb_Akxhatkk1[4];
  int32_T i;
  real_T tmp[2];
  real_T rtb_MemoryX_idx_0;
  real_T rtb_MemoryX_idx_1;
  real_T rtb_MemoryX_idx_2;
  real_T rtb_MemoryX_idx_3;
  real_T rtb_Bkuk_idx_0;
  real_T rtb_Bkuk_idx_1;
  real_T rtb_Bkuk_idx_2;
  real_T rtb_Bkuk_idx_3;
  if (rtmIsMajorTimeStep(test4_M)) {
    /* set solver stop time */
    if (!(test4_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&test4_M->solverInfo, ((test4_M->Timing.clockTickH0
        + 1) * test4_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&test4_M->solverInfo, ((test4_M->Timing.clockTick0 +
        1) * test4_M->Timing.stepSize0 + test4_M->Timing.clockTickH0 *
        test4_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(test4_M)) {
    test4_M->Timing.t[0] = rtsiGetT(&test4_M->solverInfo);
  }

  /* Reset subsysRan breadcrumbs */
  srClearBC(test4_DW.MeasurementUpdate_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(test4_DW.EnabledSubsystem_SubsysRanBC);
  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* DataStoreWrite: '<Root>/Data Store Write' */
    test4_DW.flag = 0.0;

    /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
    {
      SimStruct *rts = test4_M->childSfunctions[0];
      sfcnOutputs(rts, 1);
    }

    /* Gain: '<S2>/Encoder 500PPR' */
    for (i = 0; i < 5; i++) {
      rtb_Encoder500PPR[i] = test4_P.Encoder500PPR_Gain * test4_B.Encoder[i];
    }

    /* End of Gain: '<S2>/Encoder 500PPR' */

    /* Gain: '<S2>/Angle Scale' */
    test4_B.AngleScale = test4_P.AngleScale_Gain * rtb_Encoder500PPR[3];

    /* Sum: '<S1>/Sum' incorporates:
     *  Constant: '<S1>/pi'
     *  Constant: '<S1>/pos'
     *  Gain: '<S5>/Gain1'
     *  Product: '<S1>/Product'
     */
    rtb_ResetEncoders1 = test4_P.pi_Value * test4_P.AlfaNormalization_pos +
      test4_P.Gain1_Gain * test4_B.AngleScale;

    /* Fcn: '<S1>/angle normalization' */
    test4_B.PendPosOut = rt_atan2d_snf(sin(rtb_ResetEncoders1), cos
      (rtb_ResetEncoders1));

    /* Switch: '<Root>/Switch1' incorporates:
     *  Abs: '<Root>/Abs'
     *  Constant: '<Root>/Normal3'
     */
    if (fabs(test4_B.PendPosOut) > test4_P.Switch1_Threshold) {
      rtb_ResetEncoders1 = test4_B.PendPosOut;
    } else {
      rtb_ResetEncoders1 = test4_P.Normal3_Value;
    }

    /* End of Switch: '<Root>/Switch1' */

    /* Gain: '<Root>/Gain5' */
    test4_B.Gain5 = test4_P.K[0] * rtb_ResetEncoders1;

    /* Gain: '<S5>/Gain2' incorporates:
     *  Memory: '<S5>/Memory'
     *  Sum: '<S5>/Sum'
     */
    test4_B.Pend_vel = 0.2 * test4_P.Sensors_T0 * (test4_B.AngleScale -
      test4_DW.Memory_PreviousInput);

    /* Switch: '<Root>/Switch2' incorporates:
     *  Abs: '<Root>/Abs1'
     *  Constant: '<Root>/Normal4'
     */
    if (fabs(test4_B.Pend_vel) > test4_P.Switch2_Threshold) {
      rtb_ResetEncoders1 = test4_B.Pend_vel;
    } else {
      rtb_ResetEncoders1 = test4_P.Normal4_Value;
    }

    /* End of Switch: '<Root>/Switch2' */

    /* Gain: '<Root>/Gain6' */
    test4_B.Gain6 = test4_P.K[1] * rtb_ResetEncoders1;

    /* Sum: '<S2>/Sum' incorporates:
     *  Constant: '<S2>/Cart Offset'
     *  Gain: '<S2>/PosCart Scale1'
     */
    test4_B.Sum = test4_P.PosCartScale1_Gain * rtb_Encoder500PPR[4] +
      test4_P.CartOffset_Value;

    /* Gain: '<S5>/Gain3' */
    test4_B.Cart_pos = test4_P.Gain3_Gain_l * test4_B.Sum;

    /* Switch: '<Root>/Switch4' incorporates:
     *  Abs: '<Root>/Abs3'
     *  Constant: '<Root>/Normal6'
     */
    if (fabs(test4_B.Cart_pos) > test4_P.Switch4_Threshold) {
      rtb_ResetEncoders1 = test4_B.Cart_pos;
    } else {
      rtb_ResetEncoders1 = test4_P.Normal6_Value;
    }

    /* End of Switch: '<Root>/Switch4' */

    /* Gain: '<Root>/Gain7' */
    test4_B.Gain7 = test4_P.K[2] * rtb_ResetEncoders1;

    /* Gain: '<S5>/Gain4' incorporates:
     *  Memory: '<S5>/Memory1'
     *  Sum: '<S5>/Sum1'
     */
    test4_B.Cart_vel = 0.2 * test4_P.Sensors_T0 * (test4_B.Sum -
      test4_DW.Memory1_PreviousInput);

    /* Switch: '<Root>/Switch3' incorporates:
     *  Abs: '<Root>/Abs2'
     *  Constant: '<Root>/Normal5'
     */
    if (fabs(test4_B.Cart_vel) > test4_P.Switch3_Threshold) {
      rtb_ResetEncoders1 = test4_B.Cart_vel;
    } else {
      rtb_ResetEncoders1 = test4_P.Normal5_Value;
    }

    /* End of Switch: '<Root>/Switch3' */

    /* Gain: '<Root>/Gain8' */
    test4_B.Gain8 = test4_P.K[3] * rtb_ResetEncoders1;

    /* Sum: '<Root>/Add' */
    test4_B.Add = ((test4_B.Gain5 + test4_B.Gain6) + test4_B.Gain7) +
      test4_B.Gain8;

    /* ManualSwitch: '<Root>/Reset Encoders1' incorporates:
     *  Gain: '<Root>/Gain2'
     *  SignalConversion: '<Root>/TmpSignal ConversionAtGain2Inport1'
     */
    if (test4_P.ResetEncoders1_CurrentSetting == 1) {
      rtb_ResetEncoders1 = ((test4_P.K[0] * test4_B.PendPosOut + test4_P.K[1] *
        test4_B.Pend_vel) + test4_P.K[2] * test4_B.Cart_pos) + test4_P.K[3] *
        test4_B.Cart_vel;
    } else {
      rtb_ResetEncoders1 = test4_B.Add;
    }

    /* End of ManualSwitch: '<Root>/Reset Encoders1' */

    /* Saturate: '<Root>/Saturation' */
    if (rtb_ResetEncoders1 > test4_P.Saturation_UpperSat) {
      test4_B.sterowanie = test4_P.Saturation_UpperSat;
    } else if (rtb_ResetEncoders1 < test4_P.Saturation_LowerSat) {
      test4_B.sterowanie = test4_P.Saturation_LowerSat;
    } else {
      test4_B.sterowanie = rtb_ResetEncoders1;
    }

    /* End of Saturate: '<Root>/Saturation' */

    /* Signum: '<Root>/Sign' */
    if (test4_B.sterowanie < 0.0) {
      rtb_ResetEncoders1 = -1.0;
    } else if (test4_B.sterowanie > 0.0) {
      rtb_ResetEncoders1 = 1.0;
    } else if (test4_B.sterowanie == 0.0) {
      rtb_ResetEncoders1 = 0.0;
    } else {
      rtb_ResetEncoders1 = test4_B.sterowanie;
    }

    /* End of Signum: '<Root>/Sign' */

    /* Product: '<Root>/Product' incorporates:
     *  Fcn: '<Root>/Fcn'
     */
    rtb_Product_g = exp((fabs(test4_B.sterowanie) - 7.8335) / 3.8444) *
      rtb_ResetEncoders1;

    /* ManualSwitch: '<Root>/Turn Off Control' */
    if (test4_P.TurnOffControl_CurrentSetting == 1) {
      /* Switch: '<Root>/Switch' incorporates:
       *  Gain: '<Root>/Gain3'
       *  Gain: '<Root>/Gain4'
       */
      if (rtb_ResetEncoders1 > test4_P.Switch_Threshold) {
        test4_B.Control = test4_P.Gain3_Gain * rtb_Product_g;
      } else {
        test4_B.Control = test4_P.Gain4_Gain * rtb_Product_g;
      }
    } else {
      /* Switch: '<Root>/Switch' incorporates:
       *  Constant: '<Root>/Normal1'
       */
      test4_B.Control = test4_P.Normal1_Value;
    }

    /* End of ManualSwitch: '<Root>/Turn Off Control' */

    /* ManualSwitch: '<Root>/Turn Off Control1' incorporates:
     *  Constant: '<Root>/Normal2'
     */
    if (test4_P.TurnOffControl1_CurrentSetting == 1) {
      test4_B.TurnOffControl1 = 0.0;
    } else {
      test4_B.TurnOffControl1 = test4_P.Normal2_Value;
    }

    /* End of ManualSwitch: '<Root>/Turn Off Control1' */
  }

  /* Quantizer: '<S6>/Quantizer1' incorporates:
   *  Integrator: '<S6>/Integrator3'
   */
  test4_B.Quantizer1 = rt_roundd_snf(test4_X.Integrator3_CSTATE /
    test4_P.Quantizer1_Interval) * test4_P.Quantizer1_Interval;

  /* Derivative: '<Root>/Derivative1' */
  if ((test4_DW.TimeStampA >= test4_M->Timing.t[0]) && (test4_DW.TimeStampB >=
       test4_M->Timing.t[0])) {
    test4_B.Derivative1 = 0.0;
  } else {
    rtb_ResetEncoders1 = test4_DW.TimeStampA;
    lastU = &test4_DW.LastUAtTimeA;
    if (test4_DW.TimeStampA < test4_DW.TimeStampB) {
      if (test4_DW.TimeStampB < test4_M->Timing.t[0]) {
        rtb_ResetEncoders1 = test4_DW.TimeStampB;
        lastU = &test4_DW.LastUAtTimeB;
      }
    } else {
      if (test4_DW.TimeStampA >= test4_M->Timing.t[0]) {
        rtb_ResetEncoders1 = test4_DW.TimeStampB;
        lastU = &test4_DW.LastUAtTimeB;
      }
    }

    test4_B.Derivative1 = (test4_B.Quantizer1 - *lastU) / (test4_M->Timing.t[0]
      - rtb_ResetEncoders1);
  }

  /* End of Derivative: '<Root>/Derivative1' */

  /* Sum: '<S6>/Sum' incorporates:
   *  Constant: '<Root>/Constant'
   *  Integrator: '<S6>/Integrator1'
   */
  rtb_TrigonometricFunction1 = test4_X.Integrator1_CSTATE +
    test4_P.Constant_Value;

  /* Quantizer: '<S6>/Quantizer' */
  test4_B.Quantizer = rt_roundd_snf(rtb_TrigonometricFunction1 /
    test4_P.Quantizer_Interval) * test4_P.Quantizer_Interval;

  /* Derivative: '<Root>/Derivative3' */
  if ((test4_DW.TimeStampA_e >= test4_M->Timing.t[0]) && (test4_DW.TimeStampB_i >=
       test4_M->Timing.t[0])) {
    test4_B.Derivative3 = 0.0;
  } else {
    rtb_ResetEncoders1 = test4_DW.TimeStampA_e;
    lastU = &test4_DW.LastUAtTimeA_e;
    if (test4_DW.TimeStampA_e < test4_DW.TimeStampB_i) {
      if (test4_DW.TimeStampB_i < test4_M->Timing.t[0]) {
        rtb_ResetEncoders1 = test4_DW.TimeStampB_i;
        lastU = &test4_DW.LastUAtTimeB_e;
      }
    } else {
      if (test4_DW.TimeStampA_e >= test4_M->Timing.t[0]) {
        rtb_ResetEncoders1 = test4_DW.TimeStampB_i;
        lastU = &test4_DW.LastUAtTimeB_e;
      }
    }

    test4_B.Derivative3 = (test4_B.Quantizer - *lastU) / (test4_M->Timing.t[0] -
      rtb_ResetEncoders1);
  }

  /* End of Derivative: '<Root>/Derivative3' */
  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* ZeroOrderHold: '<Root>/Zero-Order Hold' */
    if (rtmIsMajorTimeStep(test4_M) &&
        test4_M->Timing.TaskCounters.TID[2] == 0) {
      test4_B.ZeroOrderHold = test4_B.sterowanie;

      /* ZeroOrderHold: '<Root>/Zero-Order Hold1' */
      test4_B.ZeroOrderHold1[0] = test4_B.Cart_pos;
      test4_B.ZeroOrderHold1[1] = test4_B.PendPosOut;
    }

    /* End of ZeroOrderHold: '<Root>/Zero-Order Hold' */
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[2] == 0) {
    /* Delay: '<S3>/MemoryX' incorporates:
     *  Constant: '<S3>/X0'
     */
    if (test4_DW.icLoad != 0) {
      test4_DW.MemoryX_DSTATE[0] = test4_P.X0_Value[0];
      test4_DW.MemoryX_DSTATE[1] = test4_P.X0_Value[1];
      test4_DW.MemoryX_DSTATE[2] = test4_P.X0_Value[2];
      test4_DW.MemoryX_DSTATE[3] = test4_P.X0_Value[3];
    }

    rtb_MemoryX_idx_0 = test4_DW.MemoryX_DSTATE[0];
    rtb_MemoryX_idx_1 = test4_DW.MemoryX_DSTATE[1];
    rtb_MemoryX_idx_2 = test4_DW.MemoryX_DSTATE[2];
    rtb_MemoryX_idx_3 = test4_DW.MemoryX_DSTATE[3];

    /* Outputs for Atomic SubSystem: '<S3>/UseCurrentEstimator' */
    /* Outputs for Enabled SubSystem: '<S30>/Enabled Subsystem' incorporates:
     *  EnablePort: '<S51>/Enable'
     */
    /* Constant: '<S3>/Enable' */
    if (test4_P.Enable_Value) {
      /* Sum: '<S51>/Add1' incorporates:
       *  Constant: '<S3>/C'
       *  Constant: '<S3>/D'
       *  Delay: '<S3>/MemoryX'
       *  Product: '<S51>/Product'
       *  Product: '<S51>/Product1'
       *  Product: '<S51>/Product2'
       */
      for (i = 0; i < 2; i++) {
        tmp[i] = test4_B.ZeroOrderHold1[i] - (((test4_P.C_Value[i + 2] *
          test4_DW.MemoryX_DSTATE[1] + test4_P.C_Value[i] *
          test4_DW.MemoryX_DSTATE[0]) + test4_P.C_Value[i + 4] *
          test4_DW.MemoryX_DSTATE[2]) + test4_P.C_Value[i + 6] *
          test4_DW.MemoryX_DSTATE[3]);
      }

      rtb_ResetEncoders1 = tmp[0] - test4_P.D_Value[0] * test4_B.ZeroOrderHold;
      rtb_Product_g = tmp[1] - test4_P.D_Value[1] * test4_B.ZeroOrderHold;

      /* End of Sum: '<S51>/Add1' */

      /* Product: '<S51>/Product2' incorporates:
       *  Constant: '<S7>/KalmanGainM'
       */
      for (i = 0; i < 4; i++) {
        test4_B.Product2[i] = 0.0;
        test4_B.Product2[i] += test4_P.KalmanGainM_Value[i] * rtb_ResetEncoders1;
        test4_B.Product2[i] += test4_P.KalmanGainM_Value[i + 4] * rtb_Product_g;
      }

      srUpdateBC(test4_DW.EnabledSubsystem_SubsysRanBC);
    }

    /* End of Outputs for SubSystem: '<S30>/Enabled Subsystem' */

    /* Reshape: '<S3>/Reshapexhat' incorporates:
     *  Sum: '<S30>/Add'
     */
    test4_B.Reshapexhat[0] = test4_B.Product2[0] + rtb_MemoryX_idx_0;
    test4_B.Reshapexhat[1] = test4_B.Product2[1] + rtb_MemoryX_idx_1;
    test4_B.Reshapexhat[2] = test4_B.Product2[2] + rtb_MemoryX_idx_2;
    test4_B.Reshapexhat[3] = test4_B.Product2[3] + rtb_MemoryX_idx_3;

    /* End of Outputs for SubSystem: '<S3>/UseCurrentEstimator' */
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* SignalConversion: '<Root>/TmpSignal ConversionAtTo WorkspaceInport1' */
    test4_B.TmpSignalConversionAtToWorkspaceInport1[0] = 0.0;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[1] = test4_B.Quantizer1;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[2] = test4_B.Cart_pos;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[3] = test4_B.Derivative1;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[4] = test4_B.Cart_vel;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[5] = test4_B.Quantizer;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[6] = test4_B.PendPosOut;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[7] = test4_B.Derivative3;
    test4_B.TmpSignalConversionAtToWorkspaceInport1[8] = test4_B.Pend_vel;

    /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
    {
      SimStruct *rts = test4_M->childSfunctions[1];
      sfcnOutputs(rts, 1);
    }

    /* Saturate: '<S2>/Saturation' */
    if (0.0 > test4_P.Saturation_UpperSat_c) {
      test4_B.Saturation[0] = test4_P.Saturation_UpperSat_c;
    } else if (0.0 < test4_P.Saturation_LowerSat_l) {
      test4_B.Saturation[0] = test4_P.Saturation_LowerSat_l;
    } else {
      test4_B.Saturation[0] = 0.0;
    }

    if (test4_B.Control > test4_P.Saturation_UpperSat_c) {
      test4_B.Saturation[1] = test4_P.Saturation_UpperSat_c;
    } else if (test4_B.Control < test4_P.Saturation_LowerSat_l) {
      test4_B.Saturation[1] = test4_P.Saturation_LowerSat_l;
    } else {
      test4_B.Saturation[1] = test4_B.Control;
    }

    if (0.0 > test4_P.Saturation_UpperSat_c) {
      test4_B.Saturation[2] = test4_P.Saturation_UpperSat_c;
    } else if (0.0 < test4_P.Saturation_LowerSat_l) {
      test4_B.Saturation[2] = test4_P.Saturation_LowerSat_l;
    } else {
      test4_B.Saturation[2] = 0.0;
    }

    /* End of Saturate: '<S2>/Saturation' */

    /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
    {
      SimStruct *rts = test4_M->childSfunctions[2];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSource' */
    test4_B.ResetSource[0] = test4_P.ResetSource_Value[0];
    test4_B.ResetSource[1] = test4_P.ResetSource_Value[1];
    test4_B.ResetSource[2] = test4_P.ResetSource_Value[2];

    /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[3];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/LimitFlagSource' */
    test4_B.LimitFlagSource[0] = test4_P.LimitFlagSource_Value[0];
    test4_B.LimitFlagSource[1] = test4_P.LimitFlagSource_Value[1];
    test4_B.LimitFlagSource[2] = test4_P.LimitFlagSource_Value[2];

    /* Constant: '<S2>/LimitSource' */
    test4_B.LimitSource[0] = test4_P.LimitSource_Value[0];
    test4_B.LimitSource[1] = test4_P.LimitSource_Value[1];
    test4_B.LimitSource[2] = test4_P.LimitSource_Value[2];

    /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
    {
      SimStruct *rts = test4_M->childSfunctions[4];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
    {
      SimStruct *rts = test4_M->childSfunctions[5];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
    {
      SimStruct *rts = test4_M->childSfunctions[6];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/PWMPrescalerSource' */
    test4_B.PWMPrescalerSource = test4_P.PWMPrescalerSource_Value;

    /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[7];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSwitchFlagSource' */
    test4_B.ResetSwitchFlagSource[0] = test4_P.ResetSwitchFlagSource_Value[0];
    test4_B.ResetSwitchFlagSource[1] = test4_P.ResetSwitchFlagSource_Value[1];
    test4_B.ResetSwitchFlagSource[2] = test4_P.ResetSwitchFlagSource_Value[2];

    /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[8];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ThermFlagSource' */
    test4_B.ThermFlagSource[0] = test4_P.ThermFlagSource_Value[0];
    test4_B.ThermFlagSource[1] = test4_P.ThermFlagSource_Value[1];
    test4_B.ThermFlagSource[2] = test4_P.ThermFlagSource_Value[2];

    /* Gain: '<Root>/Gain1' */
    test4_B.Gain1 = test4_P.Gain1_Gain_p * test4_B.TurnOffControl1;
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[2] == 0) {
    /* Product: '<S25>/A[k]*xhat[k|k-1]' incorporates:
     *  Constant: '<S3>/A'
     */
    for (i = 0; i < 4; i++) {
      rtb_ResetEncoders1 = test4_P.A_Value[i + 12] * rtb_MemoryX_idx_3 +
        (test4_P.A_Value[i + 8] * rtb_MemoryX_idx_2 + (test4_P.A_Value[i + 4] *
          rtb_MemoryX_idx_1 + test4_P.A_Value[i] * rtb_MemoryX_idx_0));
      rtb_Akxhatkk1[i] = rtb_ResetEncoders1;
    }

    /* End of Product: '<S25>/A[k]*xhat[k|k-1]' */

    /* Product: '<S25>/B[k]*u[k]' incorporates:
     *  Constant: '<S3>/B'
     */
    rtb_Bkuk_idx_0 = test4_P.B_Value[0] * test4_B.ZeroOrderHold;
    rtb_Bkuk_idx_1 = test4_P.B_Value[1] * test4_B.ZeroOrderHold;
    rtb_Bkuk_idx_2 = test4_P.B_Value[2] * test4_B.ZeroOrderHold;
    rtb_Bkuk_idx_3 = test4_P.B_Value[3] * test4_B.ZeroOrderHold;

    /* Outputs for Enabled SubSystem: '<S25>/MeasurementUpdate' incorporates:
     *  EnablePort: '<S50>/Enable'
     */
    /* Constant: '<S3>/Enable' */
    if (test4_P.Enable_Value) {
      /* Product: '<S25>/C[k]*xhat[k|k-1]' incorporates:
       *  Constant: '<S3>/C'
       *  Sum: '<S25>/Add1'
       */
      for (i = 0; i < 2; i++) {
        rtb_ResetEncoders1 = test4_P.C_Value[i + 6] * rtb_MemoryX_idx_3 +
          (test4_P.C_Value[i + 4] * rtb_MemoryX_idx_2 + (test4_P.C_Value[i + 2] *
            rtb_MemoryX_idx_1 + test4_P.C_Value[i] * rtb_MemoryX_idx_0));
        tmp[i] = rtb_ResetEncoders1;
      }

      /* End of Product: '<S25>/C[k]*xhat[k|k-1]' */

      /* Sum: '<S50>/Sum' incorporates:
       *  Constant: '<S3>/D'
       *  Product: '<S25>/D[k-1]*u[k-1]'
       *  Product: '<S50>/Product3'
       *  Sum: '<S25>/Add1'
       */
      rtb_ResetEncoders1 = test4_B.ZeroOrderHold1[0] - (test4_P.D_Value[0] *
        test4_B.ZeroOrderHold + tmp[0]);
      rtb_Product_g = test4_B.ZeroOrderHold1[1] - (test4_P.D_Value[1] *
        test4_B.ZeroOrderHold + tmp[1]);

      /* Product: '<S50>/Product3' incorporates:
       *  Constant: '<S7>/KalmanGainL'
       */
      for (i = 0; i < 4; i++) {
        test4_B.Product3[i] = 0.0;
        test4_B.Product3[i] += test4_P.KalmanGainL_Value[i] * rtb_ResetEncoders1;
        test4_B.Product3[i] += test4_P.KalmanGainL_Value[i + 4] * rtb_Product_g;
      }

      if (rtmIsMajorTimeStep(test4_M)) {
        srUpdateBC(test4_DW.MeasurementUpdate_SubsysRanBC);
      }
    }

    /* End of Outputs for SubSystem: '<S25>/MeasurementUpdate' */

    /* Sum: '<S25>/Add' */
    test4_B.Add_c[0] = (rtb_Bkuk_idx_0 + rtb_Akxhatkk1[0]) + test4_B.Product3[0];
    test4_B.Add_c[1] = (rtb_Bkuk_idx_1 + rtb_Akxhatkk1[1]) + test4_B.Product3[1];
    test4_B.Add_c[2] = (rtb_Bkuk_idx_2 + rtb_Akxhatkk1[2]) + test4_B.Product3[2];
    test4_B.Add_c[3] = (rtb_Bkuk_idx_3 + rtb_Akxhatkk1[3]) + test4_B.Product3[3];
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* ManualSwitch: '<Root>/Reset Encoders' incorporates:
     *  Constant: '<Root>/Normal'
     *  Constant: '<Root>/Reset'
     */
    if (test4_P.ResetEncoders_CurrentSetting == 1) {
      test4_B.ResetEncoders = test4_P.Reset_Value;
    } else {
      test4_B.ResetEncoders = test4_P.Normal_Value;
    }

    /* End of ManualSwitch: '<Root>/Reset Encoders' */

    /* Memory: '<S5>/Memory2' */
    test4_B.Memory2 = test4_DW.Memory2_PreviousInput;

    /* Memory: '<S5>/Memory3' */
    test4_B.Memory3 = test4_DW.Memory3_PreviousInput;

    /* Memory: '<S5>/Memory4' */
    test4_B.Memory4 = test4_DW.Memory4_PreviousInput;

    /* Memory: '<S5>/Memory5' */
    test4_B.Memory5 = test4_DW.Memory5_PreviousInput;

    /* Memory: '<S5>/Memory6' */
    test4_B.Memory6 = test4_DW.Memory6_PreviousInput;

    /* Memory: '<S5>/Memory7' */
    test4_B.Memory7 = test4_DW.Memory7_PreviousInput;

    /* Memory: '<S5>/Memory8' */
    test4_B.Memory8 = test4_DW.Memory8_PreviousInput;

    /* Memory: '<S5>/Memory9' */
    test4_B.Memory9 = test4_DW.Memory9_PreviousInput;

    /* Gain: '<S6>/Gain' */
    test4_B.Gain = test4_P.m * test4_P.L * test4_B.Gain1;
  }

  /* Trigonometry: '<S6>/Trigonometric Function' */
  rtb_ResetEncoders1 = cos(rtb_TrigonometricFunction1);

  /* Integrator: '<S6>/Integrator2' */
  test4_B.Integrator2 = test4_X.Integrator2_CSTATE;

  /* Trigonometry: '<S6>/Trigonometric Function1' */
  rtb_TrigonometricFunction1 = sin(rtb_TrigonometricFunction1);

  /* Integrator: '<S6>/Integrator' */
  test4_B.Integrator = test4_X.Integrator_CSTATE;

  /* Sum: '<S6>/Add1' incorporates:
   *  Constant: '<S6>/Constant'
   *  Gain: '<S6>/Gain4'
   *  Math: '<S6>/Math Function2'
   */
  rtb_Product_g = rtb_ResetEncoders1 * rtb_ResetEncoders1 * test4_P.Gain4_Gain_o
    - (test4_P.m * 0.13463762489999997 + test4_P.I) * (test4_P.M + test4_P.m);

  /* Product: '<S6>/Divide' incorporates:
   *  Gain: '<S6>/Gain5'
   *  Gain: '<S6>/Gain6'
   *  Gain: '<S6>/Gain7'
   *  Math: '<S6>/Math Function'
   *  Product: '<S6>/Product'
   *  Product: '<S6>/Product2'
   *  Product: '<S6>/Product3'
   *  Sum: '<S6>/Add'
   */
  test4_B.Divide = (((test4_B.Gain * rtb_ResetEncoders1 - test4_P.b_p *
                      test4_P.m * test4_P.L * (rtb_ResetEncoders1 *
    test4_B.Integrator2)) - (test4_P.M + test4_P.m) * test4_P.m * test4_P.L *
                     test4_P.g * rtb_TrigonometricFunction1) +
                    test4_B.Integrator * test4_B.Integrator * rtb_ResetEncoders1
                    * rtb_TrigonometricFunction1 * test4_P.Gain7_Gain) /
    rtb_Product_g;

  /* Product: '<S6>/Divide1' incorporates:
   *  Constant: '<S6>/Constant1'
   *  Gain: '<S6>/Gain1'
   *  Gain: '<S6>/Gain2'
   *  Gain: '<S6>/Gain3'
   *  Math: '<S6>/Math Function1'
   *  Product: '<S6>/Product1'
   *  Product: '<S6>/Product4'
   *  Sum: '<S6>/Add2'
   *  Sum: '<S6>/Add3'
   */
  test4_B.Divide1 = (((test4_P.b_c * test4_B.Integrator2 - test4_B.Gain1) -
                      test4_B.Integrator * test4_B.Integrator *
                      rtb_TrigonometricFunction1 * (test4_P.m * test4_P.L)) *
                     (test4_P.m * 0.13463762489999997 + test4_P.I) +
                     rtb_TrigonometricFunction1 * rtb_ResetEncoders1 *
                     (0.005385504996 * test4_P.g)) / rtb_Product_g;
}

/* Model update function */
void test4_update(void)
{
  real_T *lastU;
  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* Update for Memory: '<S5>/Memory' */
    test4_DW.Memory_PreviousInput = test4_B.Memory6;

    /* Update for Memory: '<S5>/Memory1' */
    test4_DW.Memory1_PreviousInput = test4_B.Memory2;
  }

  /* Update for Derivative: '<Root>/Derivative1' */
  if (test4_DW.TimeStampA == (rtInf)) {
    test4_DW.TimeStampA = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeA;
  } else if (test4_DW.TimeStampB == (rtInf)) {
    test4_DW.TimeStampB = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeB;
  } else if (test4_DW.TimeStampA < test4_DW.TimeStampB) {
    test4_DW.TimeStampA = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeA;
  } else {
    test4_DW.TimeStampB = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeB;
  }

  *lastU = test4_B.Quantizer1;

  /* End of Update for Derivative: '<Root>/Derivative1' */

  /* Update for Derivative: '<Root>/Derivative3' */
  if (test4_DW.TimeStampA_e == (rtInf)) {
    test4_DW.TimeStampA_e = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeA_e;
  } else if (test4_DW.TimeStampB_i == (rtInf)) {
    test4_DW.TimeStampB_i = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeB_e;
  } else if (test4_DW.TimeStampA_e < test4_DW.TimeStampB_i) {
    test4_DW.TimeStampA_e = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeA_e;
  } else {
    test4_DW.TimeStampB_i = test4_M->Timing.t[0];
    lastU = &test4_DW.LastUAtTimeB_e;
  }

  *lastU = test4_B.Quantizer;

  /* End of Update for Derivative: '<Root>/Derivative3' */
  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[2] == 0) {
    /* Update for Delay: '<S3>/MemoryX' */
    test4_DW.icLoad = 0U;
    test4_DW.MemoryX_DSTATE[0] = test4_B.Add_c[0];
    test4_DW.MemoryX_DSTATE[1] = test4_B.Add_c[1];
    test4_DW.MemoryX_DSTATE[2] = test4_B.Add_c[2];
    test4_DW.MemoryX_DSTATE[3] = test4_B.Add_c[3];
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[1] == 0) {
    /* Update for Memory: '<S5>/Memory2' */
    test4_DW.Memory2_PreviousInput = test4_B.Memory3;

    /* Update for Memory: '<S5>/Memory3' */
    test4_DW.Memory3_PreviousInput = test4_B.Memory4;

    /* Update for Memory: '<S5>/Memory4' */
    test4_DW.Memory4_PreviousInput = test4_B.Memory5;

    /* Update for Memory: '<S5>/Memory5' */
    test4_DW.Memory5_PreviousInput = test4_B.Sum;

    /* Update for Memory: '<S5>/Memory6' */
    test4_DW.Memory6_PreviousInput = test4_B.Memory7;

    /* Update for Memory: '<S5>/Memory7' */
    test4_DW.Memory7_PreviousInput = test4_B.Memory8;

    /* Update for Memory: '<S5>/Memory8' */
    test4_DW.Memory8_PreviousInput = test4_B.Memory9;

    /* Update for Memory: '<S5>/Memory9' */
    test4_DW.Memory9_PreviousInput = test4_B.AngleScale;
  }

  if (rtmIsMajorTimeStep(test4_M)) {
    rt_ertODEUpdateContinuousStates(&test4_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++test4_M->Timing.clockTick0)) {
    ++test4_M->Timing.clockTickH0;
  }

  test4_M->Timing.t[0] = rtsiGetSolverStopTime(&test4_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++test4_M->Timing.clockTick1)) {
      ++test4_M->Timing.clockTickH1;
    }

    test4_M->Timing.t[1] = test4_M->Timing.clockTick1 *
      test4_M->Timing.stepSize1 + test4_M->Timing.clockTickH1 *
      test4_M->Timing.stepSize1 * 4294967296.0;
  }

  if (rtmIsMajorTimeStep(test4_M) &&
      test4_M->Timing.TaskCounters.TID[2] == 0) {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */
    /* The "clockTick2" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick2"
     * and "Timing.stepSize2". Size of "clockTick2" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick2 and the high bits
     * Timing.clockTickH2. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++test4_M->Timing.clockTick2)) {
      ++test4_M->Timing.clockTickH2;
    }

    test4_M->Timing.t[2] = test4_M->Timing.clockTick2 *
      test4_M->Timing.stepSize2 + test4_M->Timing.clockTickH2 *
      test4_M->Timing.stepSize2 * 4294967296.0;
  }

  rate_scheduler();
}

/* Derivatives for root system: '<Root>' */
void test4_derivatives(void)
{
  XDot_test4_T *_rtXdot;
  _rtXdot = ((XDot_test4_T *) test4_M->ModelData.derivs);

  /* Derivatives for Integrator: '<S6>/Integrator3' */
  _rtXdot->Integrator3_CSTATE = test4_B.Integrator2;

  /* Derivatives for Integrator: '<S6>/Integrator1' */
  _rtXdot->Integrator1_CSTATE = test4_B.Integrator;

  /* Derivatives for Integrator: '<S6>/Integrator2' */
  _rtXdot->Integrator2_CSTATE = test4_B.Divide1;

  /* Derivatives for Integrator: '<S6>/Integrator' */
  _rtXdot->Integrator_CSTATE = test4_B.Divide;
}

/* Model initialize function */
void test4_initialize(void)
{
  /* Start for Constant: '<S2>/LimitFlagSource' */
  test4_B.LimitFlagSource[0] = test4_P.LimitFlagSource_Value[0];
  test4_B.LimitFlagSource[1] = test4_P.LimitFlagSource_Value[1];
  test4_B.LimitFlagSource[2] = test4_P.LimitFlagSource_Value[2];

  /* Start for Constant: '<S2>/LimitSource' */
  test4_B.LimitSource[0] = test4_P.LimitSource_Value[0];
  test4_B.LimitSource[1] = test4_P.LimitSource_Value[1];
  test4_B.LimitSource[2] = test4_P.LimitSource_Value[2];

  /* Start for Constant: '<S2>/PWMPrescalerSource' */
  test4_B.PWMPrescalerSource = test4_P.PWMPrescalerSource_Value;

  /* Start for Constant: '<S2>/ResetSwitchFlagSource' */
  test4_B.ResetSwitchFlagSource[0] = test4_P.ResetSwitchFlagSource_Value[0];
  test4_B.ResetSwitchFlagSource[1] = test4_P.ResetSwitchFlagSource_Value[1];
  test4_B.ResetSwitchFlagSource[2] = test4_P.ResetSwitchFlagSource_Value[2];

  /* Start for Constant: '<S2>/ThermFlagSource' */
  test4_B.ThermFlagSource[0] = test4_P.ThermFlagSource_Value[0];
  test4_B.ThermFlagSource[1] = test4_P.ThermFlagSource_Value[1];
  test4_B.ThermFlagSource[2] = test4_P.ThermFlagSource_Value[2];

  /* Start for DataStoreMemory: '<Root>/Data Store Memory' */
  test4_DW.flag = test4_P.DataStoreMemory_InitialValue;

  /* InitializeConditions for Memory: '<S5>/Memory' */
  test4_DW.Memory_PreviousInput = test4_P.Memory_X0;

  /* InitializeConditions for Memory: '<S5>/Memory1' */
  test4_DW.Memory1_PreviousInput = test4_P.Memory1_X0;

  /* InitializeConditions for Integrator: '<S6>/Integrator3' */
  test4_X.Integrator3_CSTATE = test4_P.Integrator3_IC;

  /* InitializeConditions for Derivative: '<Root>/Derivative1' */
  test4_DW.TimeStampA = (rtInf);
  test4_DW.TimeStampB = (rtInf);

  /* InitializeConditions for Integrator: '<S6>/Integrator1' */
  test4_X.Integrator1_CSTATE = test4_P.Integrator1_IC;

  /* InitializeConditions for Derivative: '<Root>/Derivative3' */
  test4_DW.TimeStampA_e = (rtInf);
  test4_DW.TimeStampB_i = (rtInf);

  /* InitializeConditions for Delay: '<S3>/MemoryX' */
  test4_DW.icLoad = 1U;

  /* InitializeConditions for Memory: '<S5>/Memory2' */
  test4_DW.Memory2_PreviousInput = test4_P.Memory2_X0;

  /* InitializeConditions for Memory: '<S5>/Memory3' */
  test4_DW.Memory3_PreviousInput = test4_P.Memory3_X0;

  /* InitializeConditions for Memory: '<S5>/Memory4' */
  test4_DW.Memory4_PreviousInput = test4_P.Memory4_X0;

  /* InitializeConditions for Memory: '<S5>/Memory5' */
  test4_DW.Memory5_PreviousInput = test4_P.Memory5_X0;

  /* InitializeConditions for Memory: '<S5>/Memory6' */
  test4_DW.Memory6_PreviousInput = test4_P.Memory6_X0;

  /* InitializeConditions for Memory: '<S5>/Memory7' */
  test4_DW.Memory7_PreviousInput = test4_P.Memory7_X0;

  /* InitializeConditions for Memory: '<S5>/Memory8' */
  test4_DW.Memory8_PreviousInput = test4_P.Memory8_X0;

  /* InitializeConditions for Memory: '<S5>/Memory9' */
  test4_DW.Memory9_PreviousInput = test4_P.Memory9_X0;

  /* InitializeConditions for Integrator: '<S6>/Integrator2' */
  test4_X.Integrator2_CSTATE = test4_P.Integrator2_IC;

  /* InitializeConditions for Integrator: '<S6>/Integrator' */
  test4_X.Integrator_CSTATE = test4_P.Integrator_IC;
}

/* Model terminate function */
void test4_terminate(void)
{
  /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
  {
    SimStruct *rts = test4_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
  {
    SimStruct *rts = test4_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
  {
    SimStruct *rts = test4_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
  {
    SimStruct *rts = test4_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
  {
    SimStruct *rts = test4_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
  {
    SimStruct *rts = test4_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
  {
    SimStruct *rts = test4_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
  {
    SimStruct *rts = test4_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
  {
    SimStruct *rts = test4_M->childSfunctions[8];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  test4_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  test4_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  test4_initialize();
}

void MdlTerminate(void)
{
  test4_terminate();
}

/* Registration function */
RT_MODEL_test4_T *test4(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)test4_M, 0,
                sizeof(RT_MODEL_test4_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&test4_M->solverInfo, &test4_M->Timing.simTimeStep);
    rtsiSetTPtr(&test4_M->solverInfo, &rtmGetTPtr(test4_M));
    rtsiSetStepSizePtr(&test4_M->solverInfo, &test4_M->Timing.stepSize0);
    rtsiSetdXPtr(&test4_M->solverInfo, &test4_M->ModelData.derivs);
    rtsiSetContStatesPtr(&test4_M->solverInfo, (real_T **)
                         &test4_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&test4_M->solverInfo, &test4_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&test4_M->solverInfo, (&rtmGetErrorStatus(test4_M)));
    rtsiSetRTModelPtr(&test4_M->solverInfo, test4_M);
  }

  rtsiSetSimTimeStep(&test4_M->solverInfo, MAJOR_TIME_STEP);
  test4_M->ModelData.intgData.y = test4_M->ModelData.odeY;
  test4_M->ModelData.intgData.f[0] = test4_M->ModelData.odeF[0];
  test4_M->ModelData.intgData.f[1] = test4_M->ModelData.odeF[1];
  test4_M->ModelData.intgData.f[2] = test4_M->ModelData.odeF[2];
  test4_M->ModelData.intgData.f[3] = test4_M->ModelData.odeF[3];
  test4_M->ModelData.intgData.f[4] = test4_M->ModelData.odeF[4];
  test4_M->ModelData.intgData.f[5] = test4_M->ModelData.odeF[5];
  test4_M->ModelData.contStates = ((real_T *) &test4_X);
  rtsiSetSolverData(&test4_M->solverInfo, (void *)&test4_M->ModelData.intgData);
  rtsiSetSolverName(&test4_M->solverInfo,"ode5");
  test4_M->solverInfoPtr = (&test4_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = test4_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    mdlTsMap[2] = 2;
    test4_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    test4_M->Timing.sampleTimes = (&test4_M->Timing.sampleTimesArray[0]);
    test4_M->Timing.offsetTimes = (&test4_M->Timing.offsetTimesArray[0]);

    /* task periods */
    test4_M->Timing.sampleTimes[0] = (0.0);
    test4_M->Timing.sampleTimes[1] = (0.002);
    test4_M->Timing.sampleTimes[2] = (0.01);

    /* task offsets */
    test4_M->Timing.offsetTimes[0] = (0.0);
    test4_M->Timing.offsetTimes[1] = (0.0);
    test4_M->Timing.offsetTimes[2] = (0.0);
  }

  rtmSetTPtr(test4_M, &test4_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = test4_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    mdlSampleHits[2] = 1;
    test4_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(test4_M, -1);
  test4_M->Timing.stepSize0 = 0.002;
  test4_M->Timing.stepSize1 = 0.002;
  test4_M->Timing.stepSize2 = 0.01;

  /* External mode info */
  test4_M->Sizes.checksums[0] = (2583260647U);
  test4_M->Sizes.checksums[1] = (1892447744U);
  test4_M->Sizes.checksums[2] = (3862569980U);
  test4_M->Sizes.checksums[3] = (1309209313U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[17];
    test4_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = &rtAlwaysEnabled;
    systemRan[7] = &rtAlwaysEnabled;
    systemRan[8] = &rtAlwaysEnabled;
    systemRan[9] = &rtAlwaysEnabled;
    systemRan[10] = &rtAlwaysEnabled;
    systemRan[11] = &rtAlwaysEnabled;
    systemRan[12] = &rtAlwaysEnabled;
    systemRan[13] = (sysRanDType *)&test4_DW.MeasurementUpdate_SubsysRanBC;
    systemRan[14] = (sysRanDType *)&test4_DW.EnabledSubsystem_SubsysRanBC;
    systemRan[15] = &rtAlwaysEnabled;
    systemRan[16] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(test4_M->extModeInfo,
      &test4_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(test4_M->extModeInfo, test4_M->Sizes.checksums);
    rteiSetTPtr(test4_M->extModeInfo, rtmGetTPtr(test4_M));
  }

  test4_M->solverInfoPtr = (&test4_M->solverInfo);
  test4_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&test4_M->solverInfo, 0.002);
  rtsiSetSolverMode(&test4_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  test4_M->ModelData.blockIO = ((void *) &test4_B);
  (void) memset(((void *) &test4_B), 0,
                sizeof(B_test4_T));

  /* parameters */
  test4_M->ModelData.defaultParam = ((real_T *)&test4_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &test4_X;
    test4_M->ModelData.contStates = (x);
    (void) memset((void *)&test4_X, 0,
                  sizeof(X_test4_T));
  }

  /* states (dwork) */
  test4_M->ModelData.dwork = ((void *) &test4_DW);
  (void) memset((void *)&test4_DW, 0,
                sizeof(DW_test4_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    test4_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &test4_M->NonInlinedSFcns.sfcnInfo;
    test4_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(test4_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &test4_M->Sizes.numSampTimes);
    test4_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(test4_M)[0]);
    test4_M->NonInlinedSFcns.taskTimePtrs[1] = &(rtmGetTPtr(test4_M)[1]);
    test4_M->NonInlinedSFcns.taskTimePtrs[2] = &(rtmGetTPtr(test4_M)[2]);
    rtssSetTPtrPtr(sfcnInfo,test4_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(test4_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(test4_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(test4_M));
    rtssSetStepSizePtr(sfcnInfo, &test4_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(test4_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &test4_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo, &test4_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &test4_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &test4_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &test4_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &test4_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &test4_M->solverInfoPtr);
  }

  test4_M->Sizes.numSFcns = (9);

  /* register each child */
  {
    (void) memset((void *)&test4_M->NonInlinedSFcns.childSFunctions[0], 0,
                  9*sizeof(SimStruct));
    test4_M->childSfunctions = (&test4_M->NonInlinedSFcns.childSFunctionPtrs[0]);

    {
      int_T i;
      for (i = 0; i < 9; i++) {
        test4_M->childSfunctions[i] = (&test4_M->
          NonInlinedSFcns.childSFunctions[i]);
      }
    }

    /* Level2 S-Function Block: test4/<S2>/Encoder (P1_Encoder) */
    {
      SimStruct *rts = test4_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.Encoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "Encoder");
      ssSetPath(rts, "test4/Cart-Pendulum System/Encoder");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.Encoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.Encoder_P2_Size);
      }

      /* registration */
      P1_Encoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test4/<S2>/PWM (P1_PWM) */
    {
      SimStruct *rts = test4_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn1.UPtrs0;
          sfcnUPtrs[0] = test4_B.Saturation;
          sfcnUPtrs[1] = &test4_B.Saturation[1];
          sfcnUPtrs[2] = &test4_B.Saturation[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.PWM));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWM");
      ssSetPath(rts, "test4/Cart-Pendulum System/PWM");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.PWM_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.PWM_P2_Size);
      }

      /* registration */
      P1_PWM(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/ResetEncoder (P1_ResetEncoder) */
    {
      SimStruct *rts = test4_M->childSfunctions[2];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[2]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[2]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[2]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[2]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn2.UPtrs0;
          sfcnUPtrs[0] = &test4_B.ResetSource[0];
          sfcnUPtrs[1] = &test4_B.ResetSource[1];
          sfcnUPtrs[2] = &test4_B.ResetSource[2];
          sfcnUPtrs[3] = &test4_B.ResetEncoders;
          sfcnUPtrs[4] = &test4_B.ResetEncoders;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 5);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.ResetEncoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetEncoder");
      ssSetPath(rts, "test4/Cart-Pendulum System/ResetEncoder");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn2.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.ResetEncoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.ResetEncoder_P2_Size);
      }

      /* registration */
      P1_ResetEncoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/LimitFlag (P1_LimitFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[3];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[3]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[3]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[3]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[3]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn3.UPtrs0;
          sfcnUPtrs[0] = test4_B.LimitFlagSource;
          sfcnUPtrs[1] = &test4_B.LimitFlagSource[1];
          sfcnUPtrs[2] = &test4_B.LimitFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn3.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.LimitFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitFlag");
      ssSetPath(rts, "test4/Cart-Pendulum System/LimitFlag");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn3.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.LimitFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.LimitFlag_P2_Size);
      }

      /* registration */
      P1_LimitFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/SetLimit (P1_SetLimit) */
    {
      SimStruct *rts = test4_M->childSfunctions[4];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[4]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[4]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[4]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[4]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn4.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn4.UPtrs0;
          sfcnUPtrs[0] = test4_B.LimitSource;
          sfcnUPtrs[1] = &test4_B.LimitSource[1];
          sfcnUPtrs[2] = &test4_B.LimitSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn4.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.SetLimit));
        }
      }

      /* path info */
      ssSetModelName(rts, "SetLimit");
      ssSetPath(rts, "test4/Cart-Pendulum System/SetLimit");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn4.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.SetLimit_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.SetLimit_P2_Size);
      }

      /* registration */
      P1_SetLimit(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/LimitSwitch (P1_Switch) */
    {
      SimStruct *rts = test4_M->childSfunctions[5];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[5]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[5]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[5]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[5]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn5.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.LimitSwitch));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitSwitch");
      ssSetPath(rts, "test4/Cart-Pendulum System/LimitSwitch");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn5.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.LimitSwitch_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.LimitSwitch_P2_Size);
      }

      /* registration */
      P1_Switch(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test4/<S2>/PWMPrescaler (P1_PWMPrescaler) */
    {
      SimStruct *rts = test4_M->childSfunctions[6];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[6]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[6]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[6]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[6]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn6.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn6.UPtrs0;
          sfcnUPtrs[0] = &test4_B.PWMPrescalerSource;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 1);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &test4_B.PWMPrescaler));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWMPrescaler");
      ssSetPath(rts, "test4/Cart-Pendulum System/PWMPrescaler");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn6.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.PWMPrescaler_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.PWMPrescaler_P2_Size);
      }

      /* registration */
      P1_PWMPrescaler(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/ResetSwitchFlag  (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[7];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[7]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[7]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[7]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[7]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn7.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn7.UPtrs0;
          sfcnUPtrs[0] = test4_B.ResetSwitchFlagSource;
          sfcnUPtrs[1] = &test4_B.ResetSwitchFlagSource[1];
          sfcnUPtrs[2] = &test4_B.ResetSwitchFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn7.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.ResetSwitchFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetSwitchFlag ");
      ssSetPath(rts, "test4/Cart-Pendulum System/ResetSwitchFlag ");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn7.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.ResetSwitchFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.ResetSwitchFlag_P2_Size);
      }

      /* registration */
      P1_ResetSwitchFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test4/<S2>/ThermFlag  (P1_ThermFlag) */
    {
      SimStruct *rts = test4_M->childSfunctions[8];

      /* timing info */
      time_T *sfcnPeriod = test4_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
      time_T *sfcnOffset = test4_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
      int_T *sfcnTsMap = test4_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test4_M->NonInlinedSFcns.blkInfo2[8]);
      }

      ssSetRTWSfcnInfo(rts, test4_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test4_M->NonInlinedSFcns.methods2[8]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test4_M->NonInlinedSFcns.methods3[8]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test4_M->NonInlinedSFcns.statesInfo2[8]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn8.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test4_M->NonInlinedSFcns.Sfcn8.UPtrs0;
          sfcnUPtrs[0] = test4_B.ThermFlagSource;
          sfcnUPtrs[1] = &test4_B.ThermFlagSource[1];
          sfcnUPtrs[2] = &test4_B.ThermFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test4_M->NonInlinedSFcns.Sfcn8.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test4_B.ThermFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ThermFlag ");
      ssSetPath(rts, "test4/Cart-Pendulum System/ThermFlag ");
      ssSetRTModel(rts,test4_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test4_M->NonInlinedSFcns.Sfcn8.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test4_P.ThermFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test4_P.ThermFlag_P2_Size);
      }

      /* registration */
      P1_ThermFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  test4_M->Sizes.numContStates = (4);  /* Number of continuous states */
  test4_M->Sizes.numPeriodicContStates = (0);/* Number of periodic continuous states */
  test4_M->Sizes.numY = (0);           /* Number of model outputs */
  test4_M->Sizes.numU = (0);           /* Number of model inputs */
  test4_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  test4_M->Sizes.numSampTimes = (3);   /* Number of sample times */
  test4_M->Sizes.numBlocks = (165);    /* Number of blocks */
  test4_M->Sizes.numBlockIO = (56);    /* Number of block outputs */
  test4_M->Sizes.numBlockPrms = (186); /* Sum of parameter "widths" */
  return test4_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
