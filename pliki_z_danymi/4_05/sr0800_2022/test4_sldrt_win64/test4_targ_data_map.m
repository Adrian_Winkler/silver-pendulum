  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 4;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test4_P)
    ;%
      section.nData     = 106;
      section.data(106)  = dumData; %prealloc
      
	  ;% test4_P.I
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_P.K
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test4_P.L
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 5;
	
	  ;% test4_P.M
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 6;
	
	  ;% test4_P.b_c
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 7;
	
	  ;% test4_P.b_p
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 8;
	
	  ;% test4_P.g
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 9;
	
	  ;% test4_P.m
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 10;
	
	  ;% test4_P.Sensors_T0
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 11;
	
	  ;% test4_P.AlfaNormalization_pos
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 12;
	
	  ;% test4_P.Normal2_Value
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 13;
	
	  ;% test4_P.Normal1_Value
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 14;
	
	  ;% test4_P.Gain4_Gain
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 15;
	
	  ;% test4_P.Gain3_Gain
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 16;
	
	  ;% test4_P.Switch_Threshold
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 17;
	
	  ;% test4_P.Normal6_Value
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 18;
	
	  ;% test4_P.Normal5_Value
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 19;
	
	  ;% test4_P.Normal4_Value
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 20;
	
	  ;% test4_P.Normal3_Value
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 21;
	
	  ;% test4_P.Normal_Value
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 22;
	
	  ;% test4_P.Reset_Value
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 23;
	
	  ;% test4_P.pi_Value
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 24;
	
	  ;% test4_P.Encoder_P1_Size
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 25;
	
	  ;% test4_P.Encoder_P1
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 27;
	
	  ;% test4_P.Encoder_P2_Size
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 28;
	
	  ;% test4_P.Encoder_P2
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 30;
	
	  ;% test4_P.Encoder500PPR_Gain
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 31;
	
	  ;% test4_P.AngleScale_Gain
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 32;
	
	  ;% test4_P.Gain1_Gain
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 33;
	
	  ;% test4_P.Switch1_Threshold
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 34;
	
	  ;% test4_P.Memory_X0
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 35;
	
	  ;% test4_P.Switch2_Threshold
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 36;
	
	  ;% test4_P.PosCartScale1_Gain
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 37;
	
	  ;% test4_P.CartOffset_Value
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 38;
	
	  ;% test4_P.Gain3_Gain_l
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 39;
	
	  ;% test4_P.Switch4_Threshold
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 40;
	
	  ;% test4_P.Memory1_X0
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 41;
	
	  ;% test4_P.Switch3_Threshold
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 42;
	
	  ;% test4_P.Saturation_UpperSat
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 43;
	
	  ;% test4_P.Saturation_LowerSat
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 44;
	
	  ;% test4_P.Integrator3_IC
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 45;
	
	  ;% test4_P.Quantizer1_Interval
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 46;
	
	  ;% test4_P.Integrator1_IC
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 47;
	
	  ;% test4_P.Constant_Value
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 48;
	
	  ;% test4_P.Quantizer_Interval
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 49;
	
	  ;% test4_P.KalmanGainM_Value
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 50;
	
	  ;% test4_P.C_Value
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 58;
	
	  ;% test4_P.D_Value
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 66;
	
	  ;% test4_P.X0_Value
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 68;
	
	  ;% test4_P.PWM_P1_Size
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 72;
	
	  ;% test4_P.PWM_P1
	  section.data(51).logicalSrcIdx = 50;
	  section.data(51).dtTransOffset = 74;
	
	  ;% test4_P.PWM_P2_Size
	  section.data(52).logicalSrcIdx = 51;
	  section.data(52).dtTransOffset = 75;
	
	  ;% test4_P.PWM_P2
	  section.data(53).logicalSrcIdx = 52;
	  section.data(53).dtTransOffset = 77;
	
	  ;% test4_P.Saturation_UpperSat_c
	  section.data(54).logicalSrcIdx = 53;
	  section.data(54).dtTransOffset = 78;
	
	  ;% test4_P.Saturation_LowerSat_l
	  section.data(55).logicalSrcIdx = 54;
	  section.data(55).dtTransOffset = 79;
	
	  ;% test4_P.ResetEncoder_P1_Size
	  section.data(56).logicalSrcIdx = 55;
	  section.data(56).dtTransOffset = 80;
	
	  ;% test4_P.ResetEncoder_P1
	  section.data(57).logicalSrcIdx = 56;
	  section.data(57).dtTransOffset = 82;
	
	  ;% test4_P.ResetEncoder_P2_Size
	  section.data(58).logicalSrcIdx = 57;
	  section.data(58).dtTransOffset = 83;
	
	  ;% test4_P.ResetEncoder_P2
	  section.data(59).logicalSrcIdx = 58;
	  section.data(59).dtTransOffset = 85;
	
	  ;% test4_P.ResetSource_Value
	  section.data(60).logicalSrcIdx = 59;
	  section.data(60).dtTransOffset = 86;
	
	  ;% test4_P.LimitFlag_P1_Size
	  section.data(61).logicalSrcIdx = 60;
	  section.data(61).dtTransOffset = 89;
	
	  ;% test4_P.LimitFlag_P1
	  section.data(62).logicalSrcIdx = 61;
	  section.data(62).dtTransOffset = 91;
	
	  ;% test4_P.LimitFlag_P2_Size
	  section.data(63).logicalSrcIdx = 62;
	  section.data(63).dtTransOffset = 92;
	
	  ;% test4_P.LimitFlag_P2
	  section.data(64).logicalSrcIdx = 63;
	  section.data(64).dtTransOffset = 94;
	
	  ;% test4_P.LimitFlagSource_Value
	  section.data(65).logicalSrcIdx = 64;
	  section.data(65).dtTransOffset = 95;
	
	  ;% test4_P.LimitSource_Value
	  section.data(66).logicalSrcIdx = 65;
	  section.data(66).dtTransOffset = 98;
	
	  ;% test4_P.SetLimit_P1_Size
	  section.data(67).logicalSrcIdx = 66;
	  section.data(67).dtTransOffset = 101;
	
	  ;% test4_P.SetLimit_P1
	  section.data(68).logicalSrcIdx = 67;
	  section.data(68).dtTransOffset = 103;
	
	  ;% test4_P.SetLimit_P2_Size
	  section.data(69).logicalSrcIdx = 68;
	  section.data(69).dtTransOffset = 104;
	
	  ;% test4_P.SetLimit_P2
	  section.data(70).logicalSrcIdx = 69;
	  section.data(70).dtTransOffset = 106;
	
	  ;% test4_P.LimitSwitch_P1_Size
	  section.data(71).logicalSrcIdx = 70;
	  section.data(71).dtTransOffset = 107;
	
	  ;% test4_P.LimitSwitch_P1
	  section.data(72).logicalSrcIdx = 71;
	  section.data(72).dtTransOffset = 109;
	
	  ;% test4_P.LimitSwitch_P2_Size
	  section.data(73).logicalSrcIdx = 72;
	  section.data(73).dtTransOffset = 110;
	
	  ;% test4_P.LimitSwitch_P2
	  section.data(74).logicalSrcIdx = 73;
	  section.data(74).dtTransOffset = 112;
	
	  ;% test4_P.PWMPrescaler_P1_Size
	  section.data(75).logicalSrcIdx = 74;
	  section.data(75).dtTransOffset = 113;
	
	  ;% test4_P.PWMPrescaler_P1
	  section.data(76).logicalSrcIdx = 75;
	  section.data(76).dtTransOffset = 115;
	
	  ;% test4_P.PWMPrescaler_P2_Size
	  section.data(77).logicalSrcIdx = 76;
	  section.data(77).dtTransOffset = 116;
	
	  ;% test4_P.PWMPrescaler_P2
	  section.data(78).logicalSrcIdx = 77;
	  section.data(78).dtTransOffset = 118;
	
	  ;% test4_P.PWMPrescalerSource_Value
	  section.data(79).logicalSrcIdx = 78;
	  section.data(79).dtTransOffset = 119;
	
	  ;% test4_P.ResetSwitchFlag_P1_Size
	  section.data(80).logicalSrcIdx = 79;
	  section.data(80).dtTransOffset = 120;
	
	  ;% test4_P.ResetSwitchFlag_P1
	  section.data(81).logicalSrcIdx = 80;
	  section.data(81).dtTransOffset = 122;
	
	  ;% test4_P.ResetSwitchFlag_P2_Size
	  section.data(82).logicalSrcIdx = 81;
	  section.data(82).dtTransOffset = 123;
	
	  ;% test4_P.ResetSwitchFlag_P2
	  section.data(83).logicalSrcIdx = 82;
	  section.data(83).dtTransOffset = 125;
	
	  ;% test4_P.ResetSwitchFlagSource_Value
	  section.data(84).logicalSrcIdx = 83;
	  section.data(84).dtTransOffset = 126;
	
	  ;% test4_P.ThermFlag_P1_Size
	  section.data(85).logicalSrcIdx = 84;
	  section.data(85).dtTransOffset = 129;
	
	  ;% test4_P.ThermFlag_P1
	  section.data(86).logicalSrcIdx = 85;
	  section.data(86).dtTransOffset = 131;
	
	  ;% test4_P.ThermFlag_P2_Size
	  section.data(87).logicalSrcIdx = 86;
	  section.data(87).dtTransOffset = 132;
	
	  ;% test4_P.ThermFlag_P2
	  section.data(88).logicalSrcIdx = 87;
	  section.data(88).dtTransOffset = 134;
	
	  ;% test4_P.ThermFlagSource_Value
	  section.data(89).logicalSrcIdx = 88;
	  section.data(89).dtTransOffset = 135;
	
	  ;% test4_P.Gain1_Gain_p
	  section.data(90).logicalSrcIdx = 89;
	  section.data(90).dtTransOffset = 138;
	
	  ;% test4_P.A_Value
	  section.data(91).logicalSrcIdx = 90;
	  section.data(91).dtTransOffset = 139;
	
	  ;% test4_P.B_Value
	  section.data(92).logicalSrcIdx = 91;
	  section.data(92).dtTransOffset = 155;
	
	  ;% test4_P.KalmanGainL_Value
	  section.data(93).logicalSrcIdx = 92;
	  section.data(93).dtTransOffset = 159;
	
	  ;% test4_P.Memory2_X0
	  section.data(94).logicalSrcIdx = 93;
	  section.data(94).dtTransOffset = 167;
	
	  ;% test4_P.Memory3_X0
	  section.data(95).logicalSrcIdx = 94;
	  section.data(95).dtTransOffset = 168;
	
	  ;% test4_P.Memory4_X0
	  section.data(96).logicalSrcIdx = 95;
	  section.data(96).dtTransOffset = 169;
	
	  ;% test4_P.Memory5_X0
	  section.data(97).logicalSrcIdx = 96;
	  section.data(97).dtTransOffset = 170;
	
	  ;% test4_P.Memory6_X0
	  section.data(98).logicalSrcIdx = 97;
	  section.data(98).dtTransOffset = 171;
	
	  ;% test4_P.Memory7_X0
	  section.data(99).logicalSrcIdx = 98;
	  section.data(99).dtTransOffset = 172;
	
	  ;% test4_P.Memory8_X0
	  section.data(100).logicalSrcIdx = 99;
	  section.data(100).dtTransOffset = 173;
	
	  ;% test4_P.Memory9_X0
	  section.data(101).logicalSrcIdx = 100;
	  section.data(101).dtTransOffset = 174;
	
	  ;% test4_P.Integrator2_IC
	  section.data(102).logicalSrcIdx = 101;
	  section.data(102).dtTransOffset = 175;
	
	  ;% test4_P.Integrator_IC
	  section.data(103).logicalSrcIdx = 102;
	  section.data(103).dtTransOffset = 176;
	
	  ;% test4_P.Gain7_Gain
	  section.data(104).logicalSrcIdx = 103;
	  section.data(104).dtTransOffset = 177;
	
	  ;% test4_P.Gain4_Gain_o
	  section.data(105).logicalSrcIdx = 104;
	  section.data(105).dtTransOffset = 178;
	
	  ;% test4_P.DataStoreMemory_InitialValue
	  section.data(106).logicalSrcIdx = 105;
	  section.data(106).dtTransOffset = 179;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% test4_P.MemoryX_DelayLength
	  section.data(1).logicalSrcIdx = 106;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% test4_P.ResetEncoders1_CurrentSetting
	  section.data(1).logicalSrcIdx = 107;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_P.TurnOffControl_CurrentSetting
	  section.data(2).logicalSrcIdx = 108;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test4_P.TurnOffControl1_CurrentSetting
	  section.data(3).logicalSrcIdx = 109;
	  section.data(3).dtTransOffset = 2;
	
	  ;% test4_P.ResetEncoders_CurrentSetting
	  section.data(4).logicalSrcIdx = 110;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% test4_P.Enable_Value
	  section.data(1).logicalSrcIdx = 111;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test4_B)
    ;%
      section.nData     = 56;
      section.data(56)  = dumData; %prealloc
      
	  ;% test4_B.Encoder
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_B.AngleScale
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 5;
	
	  ;% test4_B.PendPosOut
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 6;
	
	  ;% test4_B.Gain5
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 7;
	
	  ;% test4_B.Pend_vel
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 8;
	
	  ;% test4_B.Gain6
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 9;
	
	  ;% test4_B.Sum
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 10;
	
	  ;% test4_B.Cart_pos
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 11;
	
	  ;% test4_B.Gain7
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 12;
	
	  ;% test4_B.Cart_vel
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 13;
	
	  ;% test4_B.Gain8
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 14;
	
	  ;% test4_B.Add
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 15;
	
	  ;% test4_B.sterowanie
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 16;
	
	  ;% test4_B.Control
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 17;
	
	  ;% test4_B.TurnOffControl1
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 18;
	
	  ;% test4_B.Quantizer1
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 19;
	
	  ;% test4_B.Derivative1
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 20;
	
	  ;% test4_B.Quantizer
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 21;
	
	  ;% test4_B.Derivative3
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 22;
	
	  ;% test4_B.ZeroOrderHold
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 23;
	
	  ;% test4_B.ZeroOrderHold1
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 24;
	
	  ;% test4_B.Reshapexhat
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 26;
	
	  ;% test4_B.TmpSignalConversionAtToWorkspaceInport1
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 30;
	
	  ;% test4_B.PWM
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 39;
	
	  ;% test4_B.Saturation
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 42;
	
	  ;% test4_B.ResetEncoder
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 45;
	
	  ;% test4_B.ResetSource
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 50;
	
	  ;% test4_B.LimitFlag
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 53;
	
	  ;% test4_B.LimitFlagSource
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 56;
	
	  ;% test4_B.LimitSource
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 59;
	
	  ;% test4_B.SetLimit
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 62;
	
	  ;% test4_B.LimitSwitch
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 65;
	
	  ;% test4_B.PWMPrescaler
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 68;
	
	  ;% test4_B.PWMPrescalerSource
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 69;
	
	  ;% test4_B.ResetSwitchFlag
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 70;
	
	  ;% test4_B.ResetSwitchFlagSource
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 73;
	
	  ;% test4_B.ThermFlag
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 76;
	
	  ;% test4_B.ThermFlagSource
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 79;
	
	  ;% test4_B.Gain1
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 82;
	
	  ;% test4_B.Add_c
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 83;
	
	  ;% test4_B.ResetEncoders
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 87;
	
	  ;% test4_B.Memory2
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 88;
	
	  ;% test4_B.Memory3
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 89;
	
	  ;% test4_B.Memory4
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 90;
	
	  ;% test4_B.Memory5
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 91;
	
	  ;% test4_B.Memory6
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 92;
	
	  ;% test4_B.Memory7
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 93;
	
	  ;% test4_B.Memory8
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 94;
	
	  ;% test4_B.Memory9
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 95;
	
	  ;% test4_B.Gain
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 96;
	
	  ;% test4_B.Integrator2
	  section.data(51).logicalSrcIdx = 50;
	  section.data(51).dtTransOffset = 97;
	
	  ;% test4_B.Integrator
	  section.data(52).logicalSrcIdx = 51;
	  section.data(52).dtTransOffset = 98;
	
	  ;% test4_B.Divide
	  section.data(53).logicalSrcIdx = 52;
	  section.data(53).dtTransOffset = 99;
	
	  ;% test4_B.Divide1
	  section.data(54).logicalSrcIdx = 53;
	  section.data(54).dtTransOffset = 100;
	
	  ;% test4_B.Product2
	  section.data(55).logicalSrcIdx = 54;
	  section.data(55).dtTransOffset = 101;
	
	  ;% test4_B.Product3
	  section.data(56).logicalSrcIdx = 55;
	  section.data(56).dtTransOffset = 105;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 4;
    sectIdxOffset = 1;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test4_DW)
    ;%
      section.nData     = 20;
      section.data(20)  = dumData; %prealloc
      
	  ;% test4_DW.MemoryX_DSTATE
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_DW.Memory_PreviousInput
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 4;
	
	  ;% test4_DW.Memory1_PreviousInput
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 5;
	
	  ;% test4_DW.TimeStampA
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 6;
	
	  ;% test4_DW.LastUAtTimeA
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 7;
	
	  ;% test4_DW.TimeStampB
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 8;
	
	  ;% test4_DW.LastUAtTimeB
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 9;
	
	  ;% test4_DW.TimeStampA_e
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 10;
	
	  ;% test4_DW.LastUAtTimeA_e
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 11;
	
	  ;% test4_DW.TimeStampB_i
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 12;
	
	  ;% test4_DW.LastUAtTimeB_e
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 13;
	
	  ;% test4_DW.Memory2_PreviousInput
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 14;
	
	  ;% test4_DW.Memory3_PreviousInput
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 15;
	
	  ;% test4_DW.Memory4_PreviousInput
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 16;
	
	  ;% test4_DW.Memory5_PreviousInput
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 17;
	
	  ;% test4_DW.Memory6_PreviousInput
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 18;
	
	  ;% test4_DW.Memory7_PreviousInput
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 19;
	
	  ;% test4_DW.Memory8_PreviousInput
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 20;
	
	  ;% test4_DW.Memory9_PreviousInput
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 21;
	
	  ;% test4_DW.flag
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 22;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% test4_DW.Control_PWORK.LoggedData
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_DW.PendulumControlandStatesExperiment_PWORK.LoggedData
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test4_DW.PendulumControlandStatesExperiment1_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 2;
	
	  ;% test4_DW.Scope4_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 3;
	
	  ;% test4_DW.Scope5_PWORK.LoggedData
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 4;
	
	  ;% test4_DW.ToWorkspace_PWORK.LoggedData
	  section.data(6).logicalSrcIdx = 25;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% test4_DW.MeasurementUpdate_SubsysRanBC
	  section.data(1).logicalSrcIdx = 26;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test4_DW.EnabledSubsystem_SubsysRanBC
	  section.data(2).logicalSrcIdx = 27;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% test4_DW.icLoad
	  section.data(1).logicalSrcIdx = 28;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 2583260647;
  targMap.checksum1 = 1892447744;
  targMap.checksum2 = 3862569980;
  targMap.checksum3 = 1309209313;

