/*
 * test4_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test4".
 *
 * Model version              : 1.185
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed May 04 10:16:47 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_test4_types_h_
#define RTW_HEADER_test4_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Parameters (auto storage) */
typedef struct P_test4_T_ P_test4_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_test4_T RT_MODEL_test4_T;

#endif                                 /* RTW_HEADER_test4_types_h_ */
