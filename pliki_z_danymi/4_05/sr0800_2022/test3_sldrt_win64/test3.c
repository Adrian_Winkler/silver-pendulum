/*
 * test3.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test3".
 *
 * Model version              : 1.181
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed Apr 20 09:51:12 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "test3.h"
#include "test3_private.h"
#include "test3_dt.h"

/* list of Simulink Desktop Real-Time timers */
const int RTWinTimerCount = 1;
const double RTWinTimers[2] = {
  0.01, 0.0,
};

/* Block signals (auto storage) */
B_test3_T test3_B;

/* Continuous states */
X_test3_T test3_X;

/* Block states (auto storage) */
DW_test3_T test3_DW;

/* Real-time model */
RT_MODEL_test3_T test3_M_;
RT_MODEL_test3_T *const test3_M = &test3_M_;

/*
 * This function updates continuous states using the ODE5 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE5_A[6] = {
    1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0
  };

  static const real_T rt_ODE5_B[6][6] = {
    { 1.0/5.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

    { 3.0/40.0, 9.0/40.0, 0.0, 0.0, 0.0, 0.0 },

    { 44.0/45.0, -56.0/15.0, 32.0/9.0, 0.0, 0.0, 0.0 },

    { 19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0.0, 0.0 },

    { 9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0,
      0.0 },

    { 35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE5_IntgData *id = (ODE5_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T *f4 = id->f[4];
  real_T *f5 = id->f[5];
  real_T hB[6];
  int_T i;
  int_T nXc = 5;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  test3_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE5_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[0]);
  rtsiSetdX(si, f1);
  test3_output();
  test3_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE5_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[1]);
  rtsiSetdX(si, f2);
  test3_output();
  test3_derivatives();

  /* f(:,4) = feval(odefile, t + hA(3), y + f*hB(:,3), args(:)(*)); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE5_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[2]);
  rtsiSetdX(si, f3);
  test3_output();
  test3_derivatives();

  /* f(:,5) = feval(odefile, t + hA(4), y + f*hB(:,4), args(:)(*)); */
  for (i = 0; i <= 3; i++) {
    hB[i] = h * rt_ODE5_B[3][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[3]);
  rtsiSetdX(si, f4);
  test3_output();
  test3_derivatives();

  /* f(:,6) = feval(odefile, t + hA(5), y + f*hB(:,5), args(:)(*)); */
  for (i = 0; i <= 4; i++) {
    hB[i] = h * rt_ODE5_B[4][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f5);
  test3_output();
  test3_derivatives();

  /* tnew = t + hA(6);
     ynew = y + f*hB(:,6); */
  for (i = 0; i <= 5; i++) {
    hB[i] = h * rt_ODE5_B[5][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4] + f5[i]*hB[5]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

/* Model output function */
void test3_output(void)
{
  real_T rtb_Encoder500PPR[5];
  real_T rtb_Memory;
  real_T rtb_Integrator_f;
  int32_T i;
  if (rtmIsMajorTimeStep(test3_M)) {
    /* set solver stop time */
    if (!(test3_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&test3_M->solverInfo, ((test3_M->Timing.clockTickH0
        + 1) * test3_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&test3_M->solverInfo, ((test3_M->Timing.clockTick0 +
        1) * test3_M->Timing.stepSize0 + test3_M->Timing.clockTickH0 *
        test3_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(test3_M)) {
    test3_M->Timing.t[0] = rtsiGetT(&test3_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(test3_M)) {
    /* DataStoreWrite: '<Root>/Data Store Write' */
    test3_DW.flag = 0.0;

    /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
    {
      SimStruct *rts = test3_M->childSfunctions[0];
      sfcnOutputs(rts, 1);
    }

    /* Gain: '<S2>/Encoder 500PPR' */
    for (i = 0; i < 5; i++) {
      rtb_Encoder500PPR[i] = test3_P.Encoder500PPR_Gain * test3_B.Encoder[i];
    }

    /* End of Gain: '<S2>/Encoder 500PPR' */

    /* Gain: '<S2>/Angle Scale' */
    test3_B.AngleScale = test3_P.AngleScale_Gain * rtb_Encoder500PPR[3];

    /* Sum: '<S1>/Sum' incorporates:
     *  Constant: '<S1>/pi'
     *  Constant: '<S1>/pos'
     *  Gain: '<S6>/Gain1'
     *  Product: '<S1>/Product'
     */
    rtb_Memory = test3_P.pi_Value * test3_P.AlfaNormalization_pos +
      test3_P.Gain1_Gain_b * test3_B.AngleScale;

    /* Fcn: '<S1>/angle normalization' */
    test3_B.PendPosOut = rt_atan2d_snf(sin(rtb_Memory), cos(rtb_Memory));

    /* Sum: '<Root>/Sum' incorporates:
     *  Constant: '<Root>/Constant2'
     */
    test3_B.Sum = test3_P.Constant2_Value - test3_B.PendPosOut;

    /* Gain: '<Root>/Gain_P ' */
    test3_B.Gain_P = test3_P.Gain_P_Gain * test3_B.Sum;
  }

  /* TransferFcn: '<Root>/Transfer_D ' */
  test3_B.Transfer_D = 0.0;
  test3_B.Transfer_D += test3_P.Transfer_D_C * test3_X.Transfer_D_CSTATE;
  test3_B.Transfer_D += test3_P.Transfer_D_D * test3_B.Sum;

  /* Sum: '<Root>/Add' */
  test3_B.Add = test3_B.Gain_P + test3_B.Transfer_D;

  /* Saturate: '<Root>/Saturation' */
  if (test3_B.Add > test3_P.Saturation_UpperSat) {
    rtb_Integrator_f = test3_P.Saturation_UpperSat;
  } else if (test3_B.Add < test3_P.Saturation_LowerSat) {
    rtb_Integrator_f = test3_P.Saturation_LowerSat;
  } else {
    rtb_Integrator_f = test3_B.Add;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Fcn: '<Root>/Fcn' */
  rtb_Memory = exp((fabs(rtb_Integrator_f) - 7.8335) / 3.8444);

  /* Signum: '<Root>/Sign' */
  if (rtb_Integrator_f < 0.0) {
    rtb_Integrator_f = -1.0;
  } else if (rtb_Integrator_f > 0.0) {
    rtb_Integrator_f = 1.0;
  } else {
    if (rtb_Integrator_f == 0.0) {
      rtb_Integrator_f = 0.0;
    }
  }

  /* End of Signum: '<Root>/Sign' */

  /* Gain: '<Root>/Gain' incorporates:
   *  Product: '<Root>/Product'
   */
  rtb_Memory = rtb_Memory * rtb_Integrator_f * test3_P.Gain_Gain;

  /* Switch: '<Root>/Switch' incorporates:
   *  Gain: '<Root>/Gain1'
   */
  if (rtb_Integrator_f > test3_P.Switch_Threshold) {
    test3_B.Switch = test3_P.Gain1_Gain * rtb_Memory;
  } else {
    test3_B.Switch = rtb_Memory;
  }

  /* End of Switch: '<Root>/Switch' */

  /* ManualSwitch: '<Root>/Turn Off Control' incorporates:
   *  Constant: '<Root>/Normal1'
   */
  if (test3_P.TurnOffControl_CurrentSetting == 1) {
    test3_B.Control = test3_B.Switch;
  } else {
    test3_B.Control = test3_P.Normal1_Value;
  }

  /* End of ManualSwitch: '<Root>/Turn Off Control' */
  if (rtmIsMajorTimeStep(test3_M)) {
    /* Sum: '<S2>/Sum' incorporates:
     *  Constant: '<S2>/Cart Offset'
     *  Gain: '<S2>/PosCart Scale1'
     */
    test3_B.Sum_p = test3_P.PosCartScale1_Gain * rtb_Encoder500PPR[4] +
      test3_P.CartOffset_Value;

    /* Gain: '<S6>/Gain3' */
    test3_B.Cart_pos = test3_P.Gain3_Gain * test3_B.Sum_p;

    /* Gain: '<S6>/Gain4' incorporates:
     *  Memory: '<S6>/Memory1'
     *  Sum: '<S6>/Sum1'
     */
    test3_B.Cart_vel = 0.2 * test3_P.Sensors_T0 * (test3_B.Sum_p -
      test3_DW.Memory1_PreviousInput);

    /* Gain: '<S6>/Gain2' incorporates:
     *  Memory: '<S6>/Memory'
     *  Sum: '<S6>/Sum'
     */
    test3_B.Pend_vel = 0.2 * test3_P.Sensors_T0 * (test3_B.AngleScale -
      test3_DW.Memory_PreviousInput);

    /* Sum: '<Root>/Sum1' incorporates:
     *  Constant: '<Root>/Constant3'
     */
    test3_B.Sum1 = test3_P.Constant3_Value - test3_B.Cart_pos;

    /* Gain: '<S5>/Proportional Gain' */
    test3_B.ProportionalGain = test3_P.PIDController1_P * test3_B.Sum;

    /* Gain: '<S5>/Derivative Gain' */
    test3_B.DerivativeGain = test3_P.PIDController1_D * test3_B.Sum;
  }

  /* Gain: '<S5>/Filter Coefficient' incorporates:
   *  Integrator: '<S5>/Filter'
   *  Sum: '<S5>/SumD'
   */
  test3_B.FilterCoefficient = (test3_B.DerivativeGain - test3_X.Filter_CSTATE) *
    test3_P.PIDController1_N;

  /* Sum: '<S5>/Sum' incorporates:
   *  Integrator: '<S5>/Integrator'
   */
  test3_B.Sum_m = (test3_B.ProportionalGain + test3_X.Integrator_CSTATE) +
    test3_B.FilterCoefficient;
  if (rtmIsMajorTimeStep(test3_M)) {
    /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
    {
      SimStruct *rts = test3_M->childSfunctions[1];
      sfcnOutputs(rts, 1);
    }
  }

  /* Saturate: '<S2>/Saturation' */
  if (0.0 > test3_P.Saturation_UpperSat_c) {
    test3_B.Saturation[0] = test3_P.Saturation_UpperSat_c;
  } else if (0.0 < test3_P.Saturation_LowerSat_l) {
    test3_B.Saturation[0] = test3_P.Saturation_LowerSat_l;
  } else {
    test3_B.Saturation[0] = 0.0;
  }

  if (test3_B.Control > test3_P.Saturation_UpperSat_c) {
    test3_B.Saturation[1] = test3_P.Saturation_UpperSat_c;
  } else if (test3_B.Control < test3_P.Saturation_LowerSat_l) {
    test3_B.Saturation[1] = test3_P.Saturation_LowerSat_l;
  } else {
    test3_B.Saturation[1] = test3_B.Control;
  }

  if (0.0 > test3_P.Saturation_UpperSat_c) {
    test3_B.Saturation[2] = test3_P.Saturation_UpperSat_c;
  } else if (0.0 < test3_P.Saturation_LowerSat_l) {
    test3_B.Saturation[2] = test3_P.Saturation_LowerSat_l;
  } else {
    test3_B.Saturation[2] = 0.0;
  }

  /* End of Saturate: '<S2>/Saturation' */
  if (rtmIsMajorTimeStep(test3_M)) {
    /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
    {
      SimStruct *rts = test3_M->childSfunctions[2];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSource' */
    test3_B.ResetSource[0] = test3_P.ResetSource_Value[0];
    test3_B.ResetSource[1] = test3_P.ResetSource_Value[1];
    test3_B.ResetSource[2] = test3_P.ResetSource_Value[2];

    /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[3];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/LimitFlagSource' */
    test3_B.LimitFlagSource[0] = test3_P.LimitFlagSource_Value[0];
    test3_B.LimitFlagSource[1] = test3_P.LimitFlagSource_Value[1];
    test3_B.LimitFlagSource[2] = test3_P.LimitFlagSource_Value[2];

    /* Constant: '<S2>/LimitSource' */
    test3_B.LimitSource[0] = test3_P.LimitSource_Value[0];
    test3_B.LimitSource[1] = test3_P.LimitSource_Value[1];
    test3_B.LimitSource[2] = test3_P.LimitSource_Value[2];

    /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
    {
      SimStruct *rts = test3_M->childSfunctions[4];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
    {
      SimStruct *rts = test3_M->childSfunctions[5];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
    {
      SimStruct *rts = test3_M->childSfunctions[6];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/PWMPrescalerSource' */
    test3_B.PWMPrescalerSource = test3_P.PWMPrescalerSource_Value;

    /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[7];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSwitchFlagSource' */
    test3_B.ResetSwitchFlagSource[0] = test3_P.ResetSwitchFlagSource_Value[0];
    test3_B.ResetSwitchFlagSource[1] = test3_P.ResetSwitchFlagSource_Value[1];
    test3_B.ResetSwitchFlagSource[2] = test3_P.ResetSwitchFlagSource_Value[2];

    /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[8];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ThermFlagSource' */
    test3_B.ThermFlagSource[0] = test3_P.ThermFlagSource_Value[0];
    test3_B.ThermFlagSource[1] = test3_P.ThermFlagSource_Value[1];
    test3_B.ThermFlagSource[2] = test3_P.ThermFlagSource_Value[2];

    /* Gain: '<S4>/Derivative Gain' */
    test3_B.DerivativeGain_p = test3_P.PIDController_D * test3_B.Sum1;
  }

  /* Gain: '<S4>/Filter Coefficient' incorporates:
   *  Integrator: '<S4>/Filter'
   *  Sum: '<S4>/SumD'
   */
  test3_B.FilterCoefficient_b = (test3_B.DerivativeGain_p -
    test3_X.Filter_CSTATE_i) * test3_P.PIDController_N;
  if (rtmIsMajorTimeStep(test3_M)) {
    /* Gain: '<S4>/Integral Gain' */
    test3_B.IntegralGain = test3_P.PIDController_I * test3_B.Sum1;
  }

  if (rtmIsMajorTimeStep(test3_M)) {
    /* Gain: '<S5>/Integral Gain' */
    test3_B.IntegralGain_o = test3_P.PIDController1_I * test3_B.Sum;

    /* ManualSwitch: '<Root>/Reset Encoders' incorporates:
     *  Constant: '<Root>/Normal'
     *  Constant: '<Root>/Reset'
     */
    if (test3_P.ResetEncoders_CurrentSetting == 1) {
      test3_B.ResetEncoders = test3_P.Reset_Value;
    } else {
      test3_B.ResetEncoders = test3_P.Normal_Value;
    }

    /* End of ManualSwitch: '<Root>/Reset Encoders' */

    /* Memory: '<S6>/Memory2' */
    test3_B.Memory2 = test3_DW.Memory2_PreviousInput;

    /* Memory: '<S6>/Memory3' */
    test3_B.Memory3 = test3_DW.Memory3_PreviousInput;

    /* Memory: '<S6>/Memory4' */
    test3_B.Memory4 = test3_DW.Memory4_PreviousInput;

    /* Memory: '<S6>/Memory5' */
    test3_B.Memory5 = test3_DW.Memory5_PreviousInput;

    /* Memory: '<S6>/Memory6' */
    test3_B.Memory6 = test3_DW.Memory6_PreviousInput;

    /* Memory: '<S6>/Memory7' */
    test3_B.Memory7 = test3_DW.Memory7_PreviousInput;

    /* Memory: '<S6>/Memory8' */
    test3_B.Memory8 = test3_DW.Memory8_PreviousInput;

    /* Memory: '<S6>/Memory9' */
    test3_B.Memory9 = test3_DW.Memory9_PreviousInput;

    /* MATLAB Function 'MATLAB Function': '<S3>:1' */
  }
}

/* Model update function */
void test3_update(void)
{
  if (rtmIsMajorTimeStep(test3_M)) {
    /* Update for Memory: '<S6>/Memory1' */
    test3_DW.Memory1_PreviousInput = test3_B.Memory2;

    /* Update for Memory: '<S6>/Memory' */
    test3_DW.Memory_PreviousInput = test3_B.Memory6;

    /* Update for Memory: '<S6>/Memory2' */
    test3_DW.Memory2_PreviousInput = test3_B.Memory3;

    /* Update for Memory: '<S6>/Memory3' */
    test3_DW.Memory3_PreviousInput = test3_B.Memory4;

    /* Update for Memory: '<S6>/Memory4' */
    test3_DW.Memory4_PreviousInput = test3_B.Memory5;

    /* Update for Memory: '<S6>/Memory5' */
    test3_DW.Memory5_PreviousInput = test3_B.Sum_p;

    /* Update for Memory: '<S6>/Memory6' */
    test3_DW.Memory6_PreviousInput = test3_B.Memory7;

    /* Update for Memory: '<S6>/Memory7' */
    test3_DW.Memory7_PreviousInput = test3_B.Memory8;

    /* Update for Memory: '<S6>/Memory8' */
    test3_DW.Memory8_PreviousInput = test3_B.Memory9;

    /* Update for Memory: '<S6>/Memory9' */
    test3_DW.Memory9_PreviousInput = test3_B.AngleScale;
  }

  if (rtmIsMajorTimeStep(test3_M)) {
    rt_ertODEUpdateContinuousStates(&test3_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++test3_M->Timing.clockTick0)) {
    ++test3_M->Timing.clockTickH0;
  }

  test3_M->Timing.t[0] = rtsiGetSolverStopTime(&test3_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++test3_M->Timing.clockTick1)) {
      ++test3_M->Timing.clockTickH1;
    }

    test3_M->Timing.t[1] = test3_M->Timing.clockTick1 *
      test3_M->Timing.stepSize1 + test3_M->Timing.clockTickH1 *
      test3_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Derivatives for root system: '<Root>' */
void test3_derivatives(void)
{
  XDot_test3_T *_rtXdot;
  _rtXdot = ((XDot_test3_T *) test3_M->ModelData.derivs);

  /* Derivatives for TransferFcn: '<Root>/Transfer_D ' */
  _rtXdot->Transfer_D_CSTATE = 0.0;
  _rtXdot->Transfer_D_CSTATE += test3_P.Transfer_D_A * test3_X.Transfer_D_CSTATE;
  _rtXdot->Transfer_D_CSTATE += test3_B.Sum;

  /* Derivatives for Integrator: '<S5>/Integrator' */
  _rtXdot->Integrator_CSTATE = test3_B.IntegralGain_o;

  /* Derivatives for Integrator: '<S5>/Filter' */
  _rtXdot->Filter_CSTATE = test3_B.FilterCoefficient;

  /* Derivatives for Integrator: '<S4>/Filter' */
  _rtXdot->Filter_CSTATE_i = test3_B.FilterCoefficient_b;

  /* Derivatives for Integrator: '<S4>/Integrator' */
  _rtXdot->Integrator_CSTATE_h = test3_B.IntegralGain;
}

/* Model initialize function */
void test3_initialize(void)
{
  /* Start for Constant: '<S2>/LimitFlagSource' */
  test3_B.LimitFlagSource[0] = test3_P.LimitFlagSource_Value[0];
  test3_B.LimitFlagSource[1] = test3_P.LimitFlagSource_Value[1];
  test3_B.LimitFlagSource[2] = test3_P.LimitFlagSource_Value[2];

  /* Start for Constant: '<S2>/LimitSource' */
  test3_B.LimitSource[0] = test3_P.LimitSource_Value[0];
  test3_B.LimitSource[1] = test3_P.LimitSource_Value[1];
  test3_B.LimitSource[2] = test3_P.LimitSource_Value[2];

  /* Start for Constant: '<S2>/PWMPrescalerSource' */
  test3_B.PWMPrescalerSource = test3_P.PWMPrescalerSource_Value;

  /* Start for Constant: '<S2>/ResetSwitchFlagSource' */
  test3_B.ResetSwitchFlagSource[0] = test3_P.ResetSwitchFlagSource_Value[0];
  test3_B.ResetSwitchFlagSource[1] = test3_P.ResetSwitchFlagSource_Value[1];
  test3_B.ResetSwitchFlagSource[2] = test3_P.ResetSwitchFlagSource_Value[2];

  /* Start for Constant: '<S2>/ThermFlagSource' */
  test3_B.ThermFlagSource[0] = test3_P.ThermFlagSource_Value[0];
  test3_B.ThermFlagSource[1] = test3_P.ThermFlagSource_Value[1];
  test3_B.ThermFlagSource[2] = test3_P.ThermFlagSource_Value[2];

  /* Start for DataStoreMemory: '<Root>/Data Store Memory' */
  test3_DW.flag = test3_P.DataStoreMemory_InitialValue;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer_D ' */
  test3_X.Transfer_D_CSTATE = 0.0;

  /* InitializeConditions for Memory: '<S6>/Memory1' */
  test3_DW.Memory1_PreviousInput = test3_P.Memory1_X0;

  /* InitializeConditions for Memory: '<S6>/Memory' */
  test3_DW.Memory_PreviousInput = test3_P.Memory_X0;

  /* InitializeConditions for Integrator: '<S5>/Integrator' */
  test3_X.Integrator_CSTATE = test3_P.Integrator_IC;

  /* InitializeConditions for Integrator: '<S5>/Filter' */
  test3_X.Filter_CSTATE = test3_P.Filter_IC;

  /* InitializeConditions for Integrator: '<S4>/Filter' */
  test3_X.Filter_CSTATE_i = test3_P.Filter_IC_n;

  /* InitializeConditions for Integrator: '<S4>/Integrator' */
  test3_X.Integrator_CSTATE_h = test3_P.Integrator_IC_g;

  /* InitializeConditions for Memory: '<S6>/Memory2' */
  test3_DW.Memory2_PreviousInput = test3_P.Memory2_X0;

  /* InitializeConditions for Memory: '<S6>/Memory3' */
  test3_DW.Memory3_PreviousInput = test3_P.Memory3_X0;

  /* InitializeConditions for Memory: '<S6>/Memory4' */
  test3_DW.Memory4_PreviousInput = test3_P.Memory4_X0;

  /* InitializeConditions for Memory: '<S6>/Memory5' */
  test3_DW.Memory5_PreviousInput = test3_P.Memory5_X0;

  /* InitializeConditions for Memory: '<S6>/Memory6' */
  test3_DW.Memory6_PreviousInput = test3_P.Memory6_X0;

  /* InitializeConditions for Memory: '<S6>/Memory7' */
  test3_DW.Memory7_PreviousInput = test3_P.Memory7_X0;

  /* InitializeConditions for Memory: '<S6>/Memory8' */
  test3_DW.Memory8_PreviousInput = test3_P.Memory8_X0;

  /* InitializeConditions for Memory: '<S6>/Memory9' */
  test3_DW.Memory9_PreviousInput = test3_P.Memory9_X0;
}

/* Model terminate function */
void test3_terminate(void)
{
  /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
  {
    SimStruct *rts = test3_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
  {
    SimStruct *rts = test3_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
  {
    SimStruct *rts = test3_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
  {
    SimStruct *rts = test3_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
  {
    SimStruct *rts = test3_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
  {
    SimStruct *rts = test3_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
  {
    SimStruct *rts = test3_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
  {
    SimStruct *rts = test3_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
  {
    SimStruct *rts = test3_M->childSfunctions[8];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  test3_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  test3_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  test3_initialize();
}

void MdlTerminate(void)
{
  test3_terminate();
}

/* Registration function */
RT_MODEL_test3_T *test3(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)test3_M, 0,
                sizeof(RT_MODEL_test3_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&test3_M->solverInfo, &test3_M->Timing.simTimeStep);
    rtsiSetTPtr(&test3_M->solverInfo, &rtmGetTPtr(test3_M));
    rtsiSetStepSizePtr(&test3_M->solverInfo, &test3_M->Timing.stepSize0);
    rtsiSetdXPtr(&test3_M->solverInfo, &test3_M->ModelData.derivs);
    rtsiSetContStatesPtr(&test3_M->solverInfo, (real_T **)
                         &test3_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&test3_M->solverInfo, &test3_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&test3_M->solverInfo, (&rtmGetErrorStatus(test3_M)));
    rtsiSetRTModelPtr(&test3_M->solverInfo, test3_M);
  }

  rtsiSetSimTimeStep(&test3_M->solverInfo, MAJOR_TIME_STEP);
  test3_M->ModelData.intgData.y = test3_M->ModelData.odeY;
  test3_M->ModelData.intgData.f[0] = test3_M->ModelData.odeF[0];
  test3_M->ModelData.intgData.f[1] = test3_M->ModelData.odeF[1];
  test3_M->ModelData.intgData.f[2] = test3_M->ModelData.odeF[2];
  test3_M->ModelData.intgData.f[3] = test3_M->ModelData.odeF[3];
  test3_M->ModelData.intgData.f[4] = test3_M->ModelData.odeF[4];
  test3_M->ModelData.intgData.f[5] = test3_M->ModelData.odeF[5];
  test3_M->ModelData.contStates = ((real_T *) &test3_X);
  rtsiSetSolverData(&test3_M->solverInfo, (void *)&test3_M->ModelData.intgData);
  rtsiSetSolverName(&test3_M->solverInfo,"ode5");
  test3_M->solverInfoPtr = (&test3_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = test3_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    test3_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    test3_M->Timing.sampleTimes = (&test3_M->Timing.sampleTimesArray[0]);
    test3_M->Timing.offsetTimes = (&test3_M->Timing.offsetTimesArray[0]);

    /* task periods */
    test3_M->Timing.sampleTimes[0] = (0.0);
    test3_M->Timing.sampleTimes[1] = (0.01);

    /* task offsets */
    test3_M->Timing.offsetTimes[0] = (0.0);
    test3_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(test3_M, &test3_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = test3_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    test3_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(test3_M, -1);
  test3_M->Timing.stepSize0 = 0.01;
  test3_M->Timing.stepSize1 = 0.01;

  /* External mode info */
  test3_M->Sizes.checksums[0] = (425980938U);
  test3_M->Sizes.checksums[1] = (3377605006U);
  test3_M->Sizes.checksums[2] = (736551798U);
  test3_M->Sizes.checksums[3] = (411439909U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[5];
    test3_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(test3_M->extModeInfo,
      &test3_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(test3_M->extModeInfo, test3_M->Sizes.checksums);
    rteiSetTPtr(test3_M->extModeInfo, rtmGetTPtr(test3_M));
  }

  test3_M->solverInfoPtr = (&test3_M->solverInfo);
  test3_M->Timing.stepSize = (0.01);
  rtsiSetFixedStepSize(&test3_M->solverInfo, 0.01);
  rtsiSetSolverMode(&test3_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  test3_M->ModelData.blockIO = ((void *) &test3_B);
  (void) memset(((void *) &test3_B), 0,
                sizeof(B_test3_T));

  /* parameters */
  test3_M->ModelData.defaultParam = ((real_T *)&test3_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &test3_X;
    test3_M->ModelData.contStates = (x);
    (void) memset((void *)&test3_X, 0,
                  sizeof(X_test3_T));
  }

  /* states (dwork) */
  test3_M->ModelData.dwork = ((void *) &test3_DW);
  (void) memset((void *)&test3_DW, 0,
                sizeof(DW_test3_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    test3_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &test3_M->NonInlinedSFcns.sfcnInfo;
    test3_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(test3_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &test3_M->Sizes.numSampTimes);
    test3_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(test3_M)[0]);
    test3_M->NonInlinedSFcns.taskTimePtrs[1] = &(rtmGetTPtr(test3_M)[1]);
    rtssSetTPtrPtr(sfcnInfo,test3_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(test3_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(test3_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(test3_M));
    rtssSetStepSizePtr(sfcnInfo, &test3_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(test3_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &test3_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo, &test3_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &test3_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &test3_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &test3_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &test3_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &test3_M->solverInfoPtr);
  }

  test3_M->Sizes.numSFcns = (9);

  /* register each child */
  {
    (void) memset((void *)&test3_M->NonInlinedSFcns.childSFunctions[0], 0,
                  9*sizeof(SimStruct));
    test3_M->childSfunctions = (&test3_M->NonInlinedSFcns.childSFunctionPtrs[0]);

    {
      int_T i;
      for (i = 0; i < 9; i++) {
        test3_M->childSfunctions[i] = (&test3_M->
          NonInlinedSFcns.childSFunctions[i]);
      }
    }

    /* Level2 S-Function Block: test3/<S2>/Encoder (P1_Encoder) */
    {
      SimStruct *rts = test3_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.Encoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "Encoder");
      ssSetPath(rts, "test3/Cart-Pendulum System/Encoder");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.Encoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.Encoder_P2_Size);
      }

      /* registration */
      P1_Encoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test3/<S2>/PWM (P1_PWM) */
    {
      SimStruct *rts = test3_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn1.UPtrs0;
          sfcnUPtrs[0] = test3_B.Saturation;
          sfcnUPtrs[1] = &test3_B.Saturation[1];
          sfcnUPtrs[2] = &test3_B.Saturation[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.PWM));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWM");
      ssSetPath(rts, "test3/Cart-Pendulum System/PWM");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.PWM_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.PWM_P2_Size);
      }

      /* registration */
      P1_PWM(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/ResetEncoder (P1_ResetEncoder) */
    {
      SimStruct *rts = test3_M->childSfunctions[2];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[2]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[2]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[2]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[2]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn2.UPtrs0;
          sfcnUPtrs[0] = &test3_B.ResetSource[0];
          sfcnUPtrs[1] = &test3_B.ResetSource[1];
          sfcnUPtrs[2] = &test3_B.ResetSource[2];
          sfcnUPtrs[3] = &test3_B.ResetEncoders;
          sfcnUPtrs[4] = &test3_B.ResetEncoders;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 5);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.ResetEncoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetEncoder");
      ssSetPath(rts, "test3/Cart-Pendulum System/ResetEncoder");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn2.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.ResetEncoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.ResetEncoder_P2_Size);
      }

      /* registration */
      P1_ResetEncoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/LimitFlag (P1_LimitFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[3];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[3]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[3]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[3]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[3]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn3.UPtrs0;
          sfcnUPtrs[0] = test3_B.LimitFlagSource;
          sfcnUPtrs[1] = &test3_B.LimitFlagSource[1];
          sfcnUPtrs[2] = &test3_B.LimitFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn3.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.LimitFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitFlag");
      ssSetPath(rts, "test3/Cart-Pendulum System/LimitFlag");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn3.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.LimitFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.LimitFlag_P2_Size);
      }

      /* registration */
      P1_LimitFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/SetLimit (P1_SetLimit) */
    {
      SimStruct *rts = test3_M->childSfunctions[4];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[4]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[4]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[4]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[4]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn4.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn4.UPtrs0;
          sfcnUPtrs[0] = test3_B.LimitSource;
          sfcnUPtrs[1] = &test3_B.LimitSource[1];
          sfcnUPtrs[2] = &test3_B.LimitSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn4.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.SetLimit));
        }
      }

      /* path info */
      ssSetModelName(rts, "SetLimit");
      ssSetPath(rts, "test3/Cart-Pendulum System/SetLimit");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn4.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.SetLimit_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.SetLimit_P2_Size);
      }

      /* registration */
      P1_SetLimit(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/LimitSwitch (P1_Switch) */
    {
      SimStruct *rts = test3_M->childSfunctions[5];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[5]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[5]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[5]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[5]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn5.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.LimitSwitch));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitSwitch");
      ssSetPath(rts, "test3/Cart-Pendulum System/LimitSwitch");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn5.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.LimitSwitch_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.LimitSwitch_P2_Size);
      }

      /* registration */
      P1_Switch(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test3/<S2>/PWMPrescaler (P1_PWMPrescaler) */
    {
      SimStruct *rts = test3_M->childSfunctions[6];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[6]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[6]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[6]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[6]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn6.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn6.UPtrs0;
          sfcnUPtrs[0] = &test3_B.PWMPrescalerSource;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 1);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &test3_B.PWMPrescaler));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWMPrescaler");
      ssSetPath(rts, "test3/Cart-Pendulum System/PWMPrescaler");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn6.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.PWMPrescaler_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.PWMPrescaler_P2_Size);
      }

      /* registration */
      P1_PWMPrescaler(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/ResetSwitchFlag  (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[7];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[7]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[7]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[7]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[7]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn7.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn7.UPtrs0;
          sfcnUPtrs[0] = test3_B.ResetSwitchFlagSource;
          sfcnUPtrs[1] = &test3_B.ResetSwitchFlagSource[1];
          sfcnUPtrs[2] = &test3_B.ResetSwitchFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn7.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.ResetSwitchFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetSwitchFlag ");
      ssSetPath(rts, "test3/Cart-Pendulum System/ResetSwitchFlag ");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn7.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.ResetSwitchFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.ResetSwitchFlag_P2_Size);
      }

      /* registration */
      P1_ResetSwitchFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test3/<S2>/ThermFlag  (P1_ThermFlag) */
    {
      SimStruct *rts = test3_M->childSfunctions[8];

      /* timing info */
      time_T *sfcnPeriod = test3_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
      time_T *sfcnOffset = test3_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
      int_T *sfcnTsMap = test3_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test3_M->NonInlinedSFcns.blkInfo2[8]);
      }

      ssSetRTWSfcnInfo(rts, test3_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test3_M->NonInlinedSFcns.methods2[8]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test3_M->NonInlinedSFcns.methods3[8]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test3_M->NonInlinedSFcns.statesInfo2[8]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn8.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test3_M->NonInlinedSFcns.Sfcn8.UPtrs0;
          sfcnUPtrs[0] = test3_B.ThermFlagSource;
          sfcnUPtrs[1] = &test3_B.ThermFlagSource[1];
          sfcnUPtrs[2] = &test3_B.ThermFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test3_M->NonInlinedSFcns.Sfcn8.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test3_B.ThermFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ThermFlag ");
      ssSetPath(rts, "test3/Cart-Pendulum System/ThermFlag ");
      ssSetRTModel(rts,test3_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test3_M->NonInlinedSFcns.Sfcn8.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test3_P.ThermFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test3_P.ThermFlag_P2_Size);
      }

      /* registration */
      P1_ThermFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  test3_M->Sizes.numContStates = (5);  /* Number of continuous states */
  test3_M->Sizes.numPeriodicContStates = (0);/* Number of periodic continuous states */
  test3_M->Sizes.numY = (0);           /* Number of model outputs */
  test3_M->Sizes.numU = (0);           /* Number of model inputs */
  test3_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  test3_M->Sizes.numSampTimes = (2);   /* Number of sample times */
  test3_M->Sizes.numBlocks = (89);     /* Number of blocks */
  test3_M->Sizes.numBlockIO = (46);    /* Number of block outputs */
  test3_M->Sizes.numBlockPrms = (119); /* Sum of parameter "widths" */
  return test3_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
