/*
 * test1_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test1".
 *
 * Model version              : 1.178
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed Apr 13 09:28:35 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_test1_types_h_
#define RTW_HEADER_test1_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Parameters (auto storage) */
typedef struct P_test1_T_ P_test1_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_test1_T RT_MODEL_test1_T;

#endif                                 /* RTW_HEADER_test1_types_h_ */
