/*
 * test1.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test1".
 *
 * Model version              : 1.178
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed Apr 13 09:28:35 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "test1.h"
#include "test1_private.h"
#include "test1_dt.h"

/* list of Simulink Desktop Real-Time timers */
const int RTWinTimerCount = 1;
const double RTWinTimers[2] = {
  0.01, 0.0,
};

/* Block signals (auto storage) */
B_test1_T test1_B;

/* Continuous states */
X_test1_T test1_X;

/* Block states (auto storage) */
DW_test1_T test1_DW;

/* Real-time model */
RT_MODEL_test1_T test1_M_;
RT_MODEL_test1_T *const test1_M = &test1_M_;

/*
 * This function updates continuous states using the ODE5 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE5_A[6] = {
    1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0
  };

  static const real_T rt_ODE5_B[6][6] = {
    { 1.0/5.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

    { 3.0/40.0, 9.0/40.0, 0.0, 0.0, 0.0, 0.0 },

    { 44.0/45.0, -56.0/15.0, 32.0/9.0, 0.0, 0.0, 0.0 },

    { 19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0.0, 0.0 },

    { 9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0,
      0.0 },

    { 35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE5_IntgData *id = (ODE5_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T *f4 = id->f[4];
  real_T *f5 = id->f[5];
  real_T hB[6];
  int_T i;
  int_T nXc = 6;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  test1_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE5_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[0]);
  rtsiSetdX(si, f1);
  test1_output();
  test1_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE5_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[1]);
  rtsiSetdX(si, f2);
  test1_output();
  test1_derivatives();

  /* f(:,4) = feval(odefile, t + hA(3), y + f*hB(:,3), args(:)(*)); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE5_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[2]);
  rtsiSetdX(si, f3);
  test1_output();
  test1_derivatives();

  /* f(:,5) = feval(odefile, t + hA(4), y + f*hB(:,4), args(:)(*)); */
  for (i = 0; i <= 3; i++) {
    hB[i] = h * rt_ODE5_B[3][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[3]);
  rtsiSetdX(si, f4);
  test1_output();
  test1_derivatives();

  /* f(:,6) = feval(odefile, t + hA(5), y + f*hB(:,5), args(:)(*)); */
  for (i = 0; i <= 4; i++) {
    hB[i] = h * rt_ODE5_B[4][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f5);
  test1_output();
  test1_derivatives();

  /* tnew = t + hA(6);
     ynew = y + f*hB(:,6); */
  for (i = 0; i <= 5; i++) {
    hB[i] = h * rt_ODE5_B[5][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4] + f5[i]*hB[5]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

/* Model output function */
void test1_output(void)
{
  real_T rtb_Encoder500PPR[5];
  real_T rtb_Memory;
  int32_T i;
  real_T rtb_Memory_0;
  if (rtmIsMajorTimeStep(test1_M)) {
    /* set solver stop time */
    if (!(test1_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&test1_M->solverInfo, ((test1_M->Timing.clockTickH0
        + 1) * test1_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&test1_M->solverInfo, ((test1_M->Timing.clockTick0 +
        1) * test1_M->Timing.stepSize0 + test1_M->Timing.clockTickH0 *
        test1_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(test1_M)) {
    test1_M->Timing.t[0] = rtsiGetT(&test1_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(test1_M)) {
    /* DataStoreWrite: '<Root>/Data Store Write' */
    test1_DW.flag = 0.0;

    /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
    {
      SimStruct *rts = test1_M->childSfunctions[0];
      sfcnOutputs(rts, 1);
    }

    /* Gain: '<S2>/Encoder 500PPR' */
    for (i = 0; i < 5; i++) {
      rtb_Encoder500PPR[i] = test1_P.Encoder500PPR_Gain * test1_B.Encoder[i];
    }

    /* End of Gain: '<S2>/Encoder 500PPR' */

    /* Gain: '<S2>/Angle Scale' */
    test1_B.AngleScale = test1_P.AngleScale_Gain * rtb_Encoder500PPR[3];

    /* Sum: '<S1>/Sum' incorporates:
     *  Constant: '<S1>/pi'
     *  Constant: '<S1>/pos'
     *  Gain: '<S6>/Gain1'
     *  Product: '<S1>/Product'
     */
    rtb_Memory = test1_P.pi_Value * test1_P.AlfaNormalization_pos +
      test1_P.Gain1_Gain * test1_B.AngleScale;

    /* Fcn: '<S1>/angle normalization' */
    test1_B.PendPosOut = rt_atan2d_snf(sin(rtb_Memory), cos(rtb_Memory));

    /* Sum: '<Root>/Sum' incorporates:
     *  Constant: '<Root>/Constant2'
     */
    test1_B.Sum = test1_P.Constant2_Value - test1_B.PendPosOut;
  }

  /* Sum: '<Root>/Add2' incorporates:
   *  Constant: '<Root>/Constant4'
   *  Gain: '<Root>/GainN1'
   *  Integrator: '<Root>/Integrator1'
   */
  rtb_Memory = test1_P.GainN1_Gain * test1_X.Integrator1_CSTATE +
    test1_P.Constant4_Value;

  /* Product: '<Root>/Product1' incorporates:
   *  Constant: '<Root>/Constant1'
   *  Gain: '<Root>/GainN'
   *  Gain: '<Root>/P'
   *  Integrator: '<Root>/Integrator'
   *  Product: '<Root>/Divide1'
   *  Sum: '<Root>/Add1'
   */
  test1_B.Product1 = (test1_P.GainN_Gain * test1_X.Integrator_CSTATE +
                      test1_P.Constant1_Value) * test1_P.P_Gain / rtb_Memory *
    test1_B.Sum;
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Gain: '<Root>/D' incorporates:
     *  Constant: '<Root>/ConstantN'
     */
    test1_B.D = test1_P.D_Gain * test1_P.ConstantN_Value;
  }

  /* Product: '<Root>/Product2' incorporates:
   *  Product: '<Root>/Divide'
   */
  test1_B.Product2 = test1_B.D / rtb_Memory * test1_B.Sum;

  /* Sum: '<Root>/Add' */
  test1_B.Add = test1_B.Product1 + test1_B.Product2;

  /* Saturate: '<Root>/Saturation' */
  if (test1_B.Add > test1_P.Saturation_UpperSat) {
    rtb_Memory = test1_P.Saturation_UpperSat;
  } else if (test1_B.Add < test1_P.Saturation_LowerSat) {
    rtb_Memory = test1_P.Saturation_LowerSat;
  } else {
    rtb_Memory = test1_B.Add;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Signum: '<Root>/Sign' */
  if (rtb_Memory < 0.0) {
    rtb_Memory_0 = -1.0;
  } else if (rtb_Memory > 0.0) {
    rtb_Memory_0 = 1.0;
  } else if (rtb_Memory == 0.0) {
    rtb_Memory_0 = 0.0;
  } else {
    rtb_Memory_0 = rtb_Memory;
  }

  /* Gain: '<Root>/Gain' incorporates:
   *  Fcn: '<Root>/Fcn'
   *  Product: '<Root>/Product'
   *  Signum: '<Root>/Sign'
   */
  test1_B.Gain = exp((fabs(rtb_Memory) - 7.8335) / 3.8444) * rtb_Memory_0 *
    test1_P.Gain_Gain;

  /* ManualSwitch: '<Root>/Turn Off Control' incorporates:
   *  Constant: '<Root>/Normal1'
   */
  if (test1_P.TurnOffControl_CurrentSetting == 1) {
    test1_B.Control = test1_B.Gain;
  } else {
    test1_B.Control = test1_P.Normal1_Value;
  }

  /* End of ManualSwitch: '<Root>/Turn Off Control' */
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Sum: '<S2>/Sum' incorporates:
     *  Constant: '<S2>/Cart Offset'
     *  Gain: '<S2>/PosCart Scale1'
     */
    test1_B.Sum_p = test1_P.PosCartScale1_Gain * rtb_Encoder500PPR[4] +
      test1_P.CartOffset_Value;

    /* Gain: '<S6>/Gain3' */
    test1_B.Cart_pos = test1_P.Gain3_Gain * test1_B.Sum_p;

    /* Gain: '<S6>/Gain4' incorporates:
     *  Memory: '<S6>/Memory1'
     *  Sum: '<S6>/Sum1'
     */
    test1_B.Cart_vel = 0.2 * test1_P.Sensors_T0 * (test1_B.Sum_p -
      test1_DW.Memory1_PreviousInput);

    /* Gain: '<S6>/Gain2' incorporates:
     *  Memory: '<S6>/Memory'
     *  Sum: '<S6>/Sum'
     */
    test1_B.Pend_vel = 0.2 * test1_P.Sensors_T0 * (test1_B.AngleScale -
      test1_DW.Memory_PreviousInput);

    /* Sum: '<Root>/Sum1' incorporates:
     *  Constant: '<Root>/Constant3'
     */
    test1_B.Sum1 = test1_P.Constant3_Value - test1_B.Cart_pos;

    /* Gain: '<S5>/Proportional Gain' */
    test1_B.ProportionalGain = test1_P.PIDController1_P * test1_B.Sum;

    /* Gain: '<S5>/Derivative Gain' */
    test1_B.DerivativeGain = test1_P.PIDController1_D * test1_B.Sum;
  }

  /* Gain: '<S5>/Filter Coefficient' incorporates:
   *  Integrator: '<S5>/Filter'
   *  Sum: '<S5>/SumD'
   */
  test1_B.FilterCoefficient = (test1_B.DerivativeGain - test1_X.Filter_CSTATE) *
    test1_P.PIDController1_N;

  /* Sum: '<S5>/Sum' incorporates:
   *  Integrator: '<S5>/Integrator'
   */
  test1_B.Sum_m = (test1_B.ProportionalGain + test1_X.Integrator_CSTATE_e) +
    test1_B.FilterCoefficient;
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
    {
      SimStruct *rts = test1_M->childSfunctions[1];
      sfcnOutputs(rts, 1);
    }
  }

  /* Saturate: '<S2>/Saturation' */
  if (0.0 > test1_P.Saturation_UpperSat_c) {
    test1_B.Saturation[0] = test1_P.Saturation_UpperSat_c;
  } else if (0.0 < test1_P.Saturation_LowerSat_l) {
    test1_B.Saturation[0] = test1_P.Saturation_LowerSat_l;
  } else {
    test1_B.Saturation[0] = 0.0;
  }

  if (test1_B.Control > test1_P.Saturation_UpperSat_c) {
    test1_B.Saturation[1] = test1_P.Saturation_UpperSat_c;
  } else if (test1_B.Control < test1_P.Saturation_LowerSat_l) {
    test1_B.Saturation[1] = test1_P.Saturation_LowerSat_l;
  } else {
    test1_B.Saturation[1] = test1_B.Control;
  }

  if (0.0 > test1_P.Saturation_UpperSat_c) {
    test1_B.Saturation[2] = test1_P.Saturation_UpperSat_c;
  } else if (0.0 < test1_P.Saturation_LowerSat_l) {
    test1_B.Saturation[2] = test1_P.Saturation_LowerSat_l;
  } else {
    test1_B.Saturation[2] = 0.0;
  }

  /* End of Saturate: '<S2>/Saturation' */
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
    {
      SimStruct *rts = test1_M->childSfunctions[2];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSource' */
    test1_B.ResetSource[0] = test1_P.ResetSource_Value[0];
    test1_B.ResetSource[1] = test1_P.ResetSource_Value[1];
    test1_B.ResetSource[2] = test1_P.ResetSource_Value[2];

    /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[3];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/LimitFlagSource' */
    test1_B.LimitFlagSource[0] = test1_P.LimitFlagSource_Value[0];
    test1_B.LimitFlagSource[1] = test1_P.LimitFlagSource_Value[1];
    test1_B.LimitFlagSource[2] = test1_P.LimitFlagSource_Value[2];

    /* Constant: '<S2>/LimitSource' */
    test1_B.LimitSource[0] = test1_P.LimitSource_Value[0];
    test1_B.LimitSource[1] = test1_P.LimitSource_Value[1];
    test1_B.LimitSource[2] = test1_P.LimitSource_Value[2];

    /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
    {
      SimStruct *rts = test1_M->childSfunctions[4];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
    {
      SimStruct *rts = test1_M->childSfunctions[5];
      sfcnOutputs(rts, 1);
    }

    /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
    {
      SimStruct *rts = test1_M->childSfunctions[6];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/PWMPrescalerSource' */
    test1_B.PWMPrescalerSource = test1_P.PWMPrescalerSource_Value;

    /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[7];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ResetSwitchFlagSource' */
    test1_B.ResetSwitchFlagSource[0] = test1_P.ResetSwitchFlagSource_Value[0];
    test1_B.ResetSwitchFlagSource[1] = test1_P.ResetSwitchFlagSource_Value[1];
    test1_B.ResetSwitchFlagSource[2] = test1_P.ResetSwitchFlagSource_Value[2];

    /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[8];
      sfcnOutputs(rts, 1);
    }

    /* Constant: '<S2>/ThermFlagSource' */
    test1_B.ThermFlagSource[0] = test1_P.ThermFlagSource_Value[0];
    test1_B.ThermFlagSource[1] = test1_P.ThermFlagSource_Value[1];
    test1_B.ThermFlagSource[2] = test1_P.ThermFlagSource_Value[2];

    /* Gain: '<S4>/Derivative Gain' */
    test1_B.DerivativeGain_p = test1_P.PIDController_D * test1_B.Sum1;
  }

  /* Gain: '<S4>/Filter Coefficient' incorporates:
   *  Integrator: '<S4>/Filter'
   *  Sum: '<S4>/SumD'
   */
  test1_B.FilterCoefficient_b = (test1_B.DerivativeGain_p -
    test1_X.Filter_CSTATE_i) * test1_P.PIDController_N;
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Gain: '<S4>/Integral Gain' */
    test1_B.IntegralGain = test1_P.PIDController_I * test1_B.Sum1;
  }

  if (rtmIsMajorTimeStep(test1_M)) {
    /* Gain: '<S5>/Integral Gain' */
    test1_B.IntegralGain_o = test1_P.PIDController1_I * test1_B.Sum;

    /* ManualSwitch: '<Root>/Reset Encoders' incorporates:
     *  Constant: '<Root>/Normal'
     *  Constant: '<Root>/Reset'
     */
    if (test1_P.ResetEncoders_CurrentSetting == 1) {
      test1_B.ResetEncoders = test1_P.Reset_Value;
    } else {
      test1_B.ResetEncoders = test1_P.Normal_Value;
    }

    /* End of ManualSwitch: '<Root>/Reset Encoders' */

    /* Memory: '<S6>/Memory2' */
    test1_B.Memory2 = test1_DW.Memory2_PreviousInput;

    /* Memory: '<S6>/Memory3' */
    test1_B.Memory3 = test1_DW.Memory3_PreviousInput;

    /* Memory: '<S6>/Memory4' */
    test1_B.Memory4 = test1_DW.Memory4_PreviousInput;

    /* Memory: '<S6>/Memory5' */
    test1_B.Memory5 = test1_DW.Memory5_PreviousInput;

    /* Memory: '<S6>/Memory6' */
    test1_B.Memory6 = test1_DW.Memory6_PreviousInput;

    /* Memory: '<S6>/Memory7' */
    test1_B.Memory7 = test1_DW.Memory7_PreviousInput;

    /* Memory: '<S6>/Memory8' */
    test1_B.Memory8 = test1_DW.Memory8_PreviousInput;

    /* Memory: '<S6>/Memory9' */
    test1_B.Memory9 = test1_DW.Memory9_PreviousInput;

    /* MATLAB Function 'MATLAB Function': '<S3>:1' */
  }
}

/* Model update function */
void test1_update(void)
{
  if (rtmIsMajorTimeStep(test1_M)) {
    /* Update for Memory: '<S6>/Memory1' */
    test1_DW.Memory1_PreviousInput = test1_B.Memory2;

    /* Update for Memory: '<S6>/Memory' */
    test1_DW.Memory_PreviousInput = test1_B.Memory6;

    /* Update for Memory: '<S6>/Memory2' */
    test1_DW.Memory2_PreviousInput = test1_B.Memory3;

    /* Update for Memory: '<S6>/Memory3' */
    test1_DW.Memory3_PreviousInput = test1_B.Memory4;

    /* Update for Memory: '<S6>/Memory4' */
    test1_DW.Memory4_PreviousInput = test1_B.Memory5;

    /* Update for Memory: '<S6>/Memory5' */
    test1_DW.Memory5_PreviousInput = test1_B.Sum_p;

    /* Update for Memory: '<S6>/Memory6' */
    test1_DW.Memory6_PreviousInput = test1_B.Memory7;

    /* Update for Memory: '<S6>/Memory7' */
    test1_DW.Memory7_PreviousInput = test1_B.Memory8;

    /* Update for Memory: '<S6>/Memory8' */
    test1_DW.Memory8_PreviousInput = test1_B.Memory9;

    /* Update for Memory: '<S6>/Memory9' */
    test1_DW.Memory9_PreviousInput = test1_B.AngleScale;
  }

  if (rtmIsMajorTimeStep(test1_M)) {
    rt_ertODEUpdateContinuousStates(&test1_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++test1_M->Timing.clockTick0)) {
    ++test1_M->Timing.clockTickH0;
  }

  test1_M->Timing.t[0] = rtsiGetSolverStopTime(&test1_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++test1_M->Timing.clockTick1)) {
      ++test1_M->Timing.clockTickH1;
    }

    test1_M->Timing.t[1] = test1_M->Timing.clockTick1 *
      test1_M->Timing.stepSize1 + test1_M->Timing.clockTickH1 *
      test1_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Derivatives for root system: '<Root>' */
void test1_derivatives(void)
{
  XDot_test1_T *_rtXdot;
  _rtXdot = ((XDot_test1_T *) test1_M->ModelData.derivs);

  /* Derivatives for Integrator: '<Root>/Integrator' */
  _rtXdot->Integrator_CSTATE = test1_B.Sum;

  /* Derivatives for Integrator: '<Root>/Integrator1' */
  _rtXdot->Integrator1_CSTATE = test1_B.Sum;

  /* Derivatives for Integrator: '<S5>/Integrator' */
  _rtXdot->Integrator_CSTATE_e = test1_B.IntegralGain_o;

  /* Derivatives for Integrator: '<S5>/Filter' */
  _rtXdot->Filter_CSTATE = test1_B.FilterCoefficient;

  /* Derivatives for Integrator: '<S4>/Filter' */
  _rtXdot->Filter_CSTATE_i = test1_B.FilterCoefficient_b;

  /* Derivatives for Integrator: '<S4>/Integrator' */
  _rtXdot->Integrator_CSTATE_h = test1_B.IntegralGain;
}

/* Model initialize function */
void test1_initialize(void)
{
  /* Start for Constant: '<S2>/LimitFlagSource' */
  test1_B.LimitFlagSource[0] = test1_P.LimitFlagSource_Value[0];
  test1_B.LimitFlagSource[1] = test1_P.LimitFlagSource_Value[1];
  test1_B.LimitFlagSource[2] = test1_P.LimitFlagSource_Value[2];

  /* Start for Constant: '<S2>/LimitSource' */
  test1_B.LimitSource[0] = test1_P.LimitSource_Value[0];
  test1_B.LimitSource[1] = test1_P.LimitSource_Value[1];
  test1_B.LimitSource[2] = test1_P.LimitSource_Value[2];

  /* Start for Constant: '<S2>/PWMPrescalerSource' */
  test1_B.PWMPrescalerSource = test1_P.PWMPrescalerSource_Value;

  /* Start for Constant: '<S2>/ResetSwitchFlagSource' */
  test1_B.ResetSwitchFlagSource[0] = test1_P.ResetSwitchFlagSource_Value[0];
  test1_B.ResetSwitchFlagSource[1] = test1_P.ResetSwitchFlagSource_Value[1];
  test1_B.ResetSwitchFlagSource[2] = test1_P.ResetSwitchFlagSource_Value[2];

  /* Start for Constant: '<S2>/ThermFlagSource' */
  test1_B.ThermFlagSource[0] = test1_P.ThermFlagSource_Value[0];
  test1_B.ThermFlagSource[1] = test1_P.ThermFlagSource_Value[1];
  test1_B.ThermFlagSource[2] = test1_P.ThermFlagSource_Value[2];

  /* Start for DataStoreMemory: '<Root>/Data Store Memory' */
  test1_DW.flag = test1_P.DataStoreMemory_InitialValue;

  /* InitializeConditions for Integrator: '<Root>/Integrator' */
  test1_X.Integrator_CSTATE = test1_P.Integrator_IC;

  /* InitializeConditions for Integrator: '<Root>/Integrator1' */
  test1_X.Integrator1_CSTATE = test1_P.Integrator1_IC;

  /* InitializeConditions for Memory: '<S6>/Memory1' */
  test1_DW.Memory1_PreviousInput = test1_P.Memory1_X0;

  /* InitializeConditions for Memory: '<S6>/Memory' */
  test1_DW.Memory_PreviousInput = test1_P.Memory_X0;

  /* InitializeConditions for Integrator: '<S5>/Integrator' */
  test1_X.Integrator_CSTATE_e = test1_P.Integrator_IC_j;

  /* InitializeConditions for Integrator: '<S5>/Filter' */
  test1_X.Filter_CSTATE = test1_P.Filter_IC;

  /* InitializeConditions for Integrator: '<S4>/Filter' */
  test1_X.Filter_CSTATE_i = test1_P.Filter_IC_n;

  /* InitializeConditions for Integrator: '<S4>/Integrator' */
  test1_X.Integrator_CSTATE_h = test1_P.Integrator_IC_g;

  /* InitializeConditions for Memory: '<S6>/Memory2' */
  test1_DW.Memory2_PreviousInput = test1_P.Memory2_X0;

  /* InitializeConditions for Memory: '<S6>/Memory3' */
  test1_DW.Memory3_PreviousInput = test1_P.Memory3_X0;

  /* InitializeConditions for Memory: '<S6>/Memory4' */
  test1_DW.Memory4_PreviousInput = test1_P.Memory4_X0;

  /* InitializeConditions for Memory: '<S6>/Memory5' */
  test1_DW.Memory5_PreviousInput = test1_P.Memory5_X0;

  /* InitializeConditions for Memory: '<S6>/Memory6' */
  test1_DW.Memory6_PreviousInput = test1_P.Memory6_X0;

  /* InitializeConditions for Memory: '<S6>/Memory7' */
  test1_DW.Memory7_PreviousInput = test1_P.Memory7_X0;

  /* InitializeConditions for Memory: '<S6>/Memory8' */
  test1_DW.Memory8_PreviousInput = test1_P.Memory8_X0;

  /* InitializeConditions for Memory: '<S6>/Memory9' */
  test1_DW.Memory9_PreviousInput = test1_P.Memory9_X0;
}

/* Model terminate function */
void test1_terminate(void)
{
  /* Level2 S-Function Block: '<S2>/Encoder' (P1_Encoder) */
  {
    SimStruct *rts = test1_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWM' (P1_PWM) */
  {
    SimStruct *rts = test1_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetEncoder' (P1_ResetEncoder) */
  {
    SimStruct *rts = test1_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitFlag' (P1_LimitFlag) */
  {
    SimStruct *rts = test1_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/SetLimit' (P1_SetLimit) */
  {
    SimStruct *rts = test1_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/LimitSwitch' (P1_Switch) */
  {
    SimStruct *rts = test1_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/PWMPrescaler' (P1_PWMPrescaler) */
  {
    SimStruct *rts = test1_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ResetSwitchFlag ' (P1_ResetSwitchFlag) */
  {
    SimStruct *rts = test1_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/ThermFlag ' (P1_ThermFlag) */
  {
    SimStruct *rts = test1_M->childSfunctions[8];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  test1_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  test1_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  test1_initialize();
}

void MdlTerminate(void)
{
  test1_terminate();
}

/* Registration function */
RT_MODEL_test1_T *test1(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)test1_M, 0,
                sizeof(RT_MODEL_test1_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&test1_M->solverInfo, &test1_M->Timing.simTimeStep);
    rtsiSetTPtr(&test1_M->solverInfo, &rtmGetTPtr(test1_M));
    rtsiSetStepSizePtr(&test1_M->solverInfo, &test1_M->Timing.stepSize0);
    rtsiSetdXPtr(&test1_M->solverInfo, &test1_M->ModelData.derivs);
    rtsiSetContStatesPtr(&test1_M->solverInfo, (real_T **)
                         &test1_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&test1_M->solverInfo, &test1_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&test1_M->solverInfo, (&rtmGetErrorStatus(test1_M)));
    rtsiSetRTModelPtr(&test1_M->solverInfo, test1_M);
  }

  rtsiSetSimTimeStep(&test1_M->solverInfo, MAJOR_TIME_STEP);
  test1_M->ModelData.intgData.y = test1_M->ModelData.odeY;
  test1_M->ModelData.intgData.f[0] = test1_M->ModelData.odeF[0];
  test1_M->ModelData.intgData.f[1] = test1_M->ModelData.odeF[1];
  test1_M->ModelData.intgData.f[2] = test1_M->ModelData.odeF[2];
  test1_M->ModelData.intgData.f[3] = test1_M->ModelData.odeF[3];
  test1_M->ModelData.intgData.f[4] = test1_M->ModelData.odeF[4];
  test1_M->ModelData.intgData.f[5] = test1_M->ModelData.odeF[5];
  test1_M->ModelData.contStates = ((real_T *) &test1_X);
  rtsiSetSolverData(&test1_M->solverInfo, (void *)&test1_M->ModelData.intgData);
  rtsiSetSolverName(&test1_M->solverInfo,"ode5");
  test1_M->solverInfoPtr = (&test1_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = test1_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    test1_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    test1_M->Timing.sampleTimes = (&test1_M->Timing.sampleTimesArray[0]);
    test1_M->Timing.offsetTimes = (&test1_M->Timing.offsetTimesArray[0]);

    /* task periods */
    test1_M->Timing.sampleTimes[0] = (0.0);
    test1_M->Timing.sampleTimes[1] = (0.01);

    /* task offsets */
    test1_M->Timing.offsetTimes[0] = (0.0);
    test1_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(test1_M, &test1_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = test1_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    test1_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(test1_M, -1);
  test1_M->Timing.stepSize0 = 0.01;
  test1_M->Timing.stepSize1 = 0.01;

  /* External mode info */
  test1_M->Sizes.checksums[0] = (339807993U);
  test1_M->Sizes.checksums[1] = (427003870U);
  test1_M->Sizes.checksums[2] = (1105148219U);
  test1_M->Sizes.checksums[3] = (1284371365U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[4];
    test1_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(test1_M->extModeInfo,
      &test1_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(test1_M->extModeInfo, test1_M->Sizes.checksums);
    rteiSetTPtr(test1_M->extModeInfo, rtmGetTPtr(test1_M));
  }

  test1_M->solverInfoPtr = (&test1_M->solverInfo);
  test1_M->Timing.stepSize = (0.01);
  rtsiSetFixedStepSize(&test1_M->solverInfo, 0.01);
  rtsiSetSolverMode(&test1_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  test1_M->ModelData.blockIO = ((void *) &test1_B);
  (void) memset(((void *) &test1_B), 0,
                sizeof(B_test1_T));

  /* parameters */
  test1_M->ModelData.defaultParam = ((real_T *)&test1_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &test1_X;
    test1_M->ModelData.contStates = (x);
    (void) memset((void *)&test1_X, 0,
                  sizeof(X_test1_T));
  }

  /* states (dwork) */
  test1_M->ModelData.dwork = ((void *) &test1_DW);
  (void) memset((void *)&test1_DW, 0,
                sizeof(DW_test1_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    test1_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &test1_M->NonInlinedSFcns.sfcnInfo;
    test1_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(test1_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &test1_M->Sizes.numSampTimes);
    test1_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(test1_M)[0]);
    test1_M->NonInlinedSFcns.taskTimePtrs[1] = &(rtmGetTPtr(test1_M)[1]);
    rtssSetTPtrPtr(sfcnInfo,test1_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(test1_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(test1_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(test1_M));
    rtssSetStepSizePtr(sfcnInfo, &test1_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(test1_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &test1_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo, &test1_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &test1_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &test1_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &test1_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &test1_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &test1_M->solverInfoPtr);
  }

  test1_M->Sizes.numSFcns = (9);

  /* register each child */
  {
    (void) memset((void *)&test1_M->NonInlinedSFcns.childSFunctions[0], 0,
                  9*sizeof(SimStruct));
    test1_M->childSfunctions = (&test1_M->NonInlinedSFcns.childSFunctionPtrs[0]);

    {
      int_T i;
      for (i = 0; i < 9; i++) {
        test1_M->childSfunctions[i] = (&test1_M->
          NonInlinedSFcns.childSFunctions[i]);
      }
    }

    /* Level2 S-Function Block: test1/<S2>/Encoder (P1_Encoder) */
    {
      SimStruct *rts = test1_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.Encoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "Encoder");
      ssSetPath(rts, "test1/Cart-Pendulum System/Encoder");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.Encoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.Encoder_P2_Size);
      }

      /* registration */
      P1_Encoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test1/<S2>/PWM (P1_PWM) */
    {
      SimStruct *rts = test1_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn1.UPtrs0;
          sfcnUPtrs[0] = test1_B.Saturation;
          sfcnUPtrs[1] = &test1_B.Saturation[1];
          sfcnUPtrs[2] = &test1_B.Saturation[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.PWM));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWM");
      ssSetPath(rts, "test1/Cart-Pendulum System/PWM");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.PWM_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.PWM_P2_Size);
      }

      /* registration */
      P1_PWM(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/ResetEncoder (P1_ResetEncoder) */
    {
      SimStruct *rts = test1_M->childSfunctions[2];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[2]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[2]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[2]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[2]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn2.UPtrs0;
          sfcnUPtrs[0] = &test1_B.ResetSource[0];
          sfcnUPtrs[1] = &test1_B.ResetSource[1];
          sfcnUPtrs[2] = &test1_B.ResetSource[2];
          sfcnUPtrs[3] = &test1_B.ResetEncoders;
          sfcnUPtrs[4] = &test1_B.ResetEncoders;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 5);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 5);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.ResetEncoder));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetEncoder");
      ssSetPath(rts, "test1/Cart-Pendulum System/ResetEncoder");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn2.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.ResetEncoder_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.ResetEncoder_P2_Size);
      }

      /* registration */
      P1_ResetEncoder(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/LimitFlag (P1_LimitFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[3];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[3]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[3]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[3]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[3]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn3.UPtrs0;
          sfcnUPtrs[0] = test1_B.LimitFlagSource;
          sfcnUPtrs[1] = &test1_B.LimitFlagSource[1];
          sfcnUPtrs[2] = &test1_B.LimitFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn3.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.LimitFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitFlag");
      ssSetPath(rts, "test1/Cart-Pendulum System/LimitFlag");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn3.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.LimitFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.LimitFlag_P2_Size);
      }

      /* registration */
      P1_LimitFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/SetLimit (P1_SetLimit) */
    {
      SimStruct *rts = test1_M->childSfunctions[4];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[4]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[4]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[4]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[4]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn4.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn4.UPtrs0;
          sfcnUPtrs[0] = test1_B.LimitSource;
          sfcnUPtrs[1] = &test1_B.LimitSource[1];
          sfcnUPtrs[2] = &test1_B.LimitSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn4.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.SetLimit));
        }
      }

      /* path info */
      ssSetModelName(rts, "SetLimit");
      ssSetPath(rts, "test1/Cart-Pendulum System/SetLimit");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn4.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.SetLimit_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.SetLimit_P2_Size);
      }

      /* registration */
      P1_SetLimit(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/LimitSwitch (P1_Switch) */
    {
      SimStruct *rts = test1_M->childSfunctions[5];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[5]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[5]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[5]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[5]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn5.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.LimitSwitch));
        }
      }

      /* path info */
      ssSetModelName(rts, "LimitSwitch");
      ssSetPath(rts, "test1/Cart-Pendulum System/LimitSwitch");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn5.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.LimitSwitch_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.LimitSwitch_P2_Size);
      }

      /* registration */
      P1_Switch(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: test1/<S2>/PWMPrescaler (P1_PWMPrescaler) */
    {
      SimStruct *rts = test1_M->childSfunctions[6];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[6]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[6]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[6]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[6]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn6.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn6.UPtrs0;
          sfcnUPtrs[0] = &test1_B.PWMPrescalerSource;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 1);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &test1_B.PWMPrescaler));
        }
      }

      /* path info */
      ssSetModelName(rts, "PWMPrescaler");
      ssSetPath(rts, "test1/Cart-Pendulum System/PWMPrescaler");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn6.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.PWMPrescaler_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.PWMPrescaler_P2_Size);
      }

      /* registration */
      P1_PWMPrescaler(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/ResetSwitchFlag  (P1_ResetSwitchFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[7];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[7]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[7]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[7]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[7]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn7.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn7.UPtrs0;
          sfcnUPtrs[0] = test1_B.ResetSwitchFlagSource;
          sfcnUPtrs[1] = &test1_B.ResetSwitchFlagSource[1];
          sfcnUPtrs[2] = &test1_B.ResetSwitchFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn7.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.ResetSwitchFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ResetSwitchFlag ");
      ssSetPath(rts, "test1/Cart-Pendulum System/ResetSwitchFlag ");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn7.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.ResetSwitchFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.ResetSwitchFlag_P2_Size);
      }

      /* registration */
      P1_ResetSwitchFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: test1/<S2>/ThermFlag  (P1_ThermFlag) */
    {
      SimStruct *rts = test1_M->childSfunctions[8];

      /* timing info */
      time_T *sfcnPeriod = test1_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
      time_T *sfcnOffset = test1_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
      int_T *sfcnTsMap = test1_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &test1_M->NonInlinedSFcns.blkInfo2[8]);
      }

      ssSetRTWSfcnInfo(rts, test1_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &test1_M->NonInlinedSFcns.methods2[8]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &test1_M->NonInlinedSFcns.methods3[8]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &test1_M->NonInlinedSFcns.statesInfo2[8]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn8.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &test1_M->NonInlinedSFcns.Sfcn8.UPtrs0;
          sfcnUPtrs[0] = test1_B.ThermFlagSource;
          sfcnUPtrs[1] = &test1_B.ThermFlagSource[1];
          sfcnUPtrs[2] = &test1_B.ThermFlagSource[2];
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &test1_M->NonInlinedSFcns.Sfcn8.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 3);
          ssSetOutputPortSignal(rts, 0, ((real_T *) test1_B.ThermFlag));
        }
      }

      /* path info */
      ssSetModelName(rts, "ThermFlag ");
      ssSetPath(rts, "test1/Cart-Pendulum System/ThermFlag ");
      ssSetRTModel(rts,test1_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &test1_M->NonInlinedSFcns.Sfcn8.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)test1_P.ThermFlag_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)test1_P.ThermFlag_P2_Size);
      }

      /* registration */
      P1_ThermFlag(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 1;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  test1_M->Sizes.numContStates = (6);  /* Number of continuous states */
  test1_M->Sizes.numPeriodicContStates = (0);/* Number of periodic continuous states */
  test1_M->Sizes.numY = (0);           /* Number of model outputs */
  test1_M->Sizes.numU = (0);           /* Number of model inputs */
  test1_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  test1_M->Sizes.numSampTimes = (2);   /* Number of sample times */
  test1_M->Sizes.numBlocks = (99);     /* Number of blocks */
  test1_M->Sizes.numBlockIO = (47);    /* Number of block outputs */
  test1_M->Sizes.numBlockPrms = (122); /* Sum of parameter "widths" */
  return test1_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
