n = P1_ExpData.signals
t = P1_ExpData.time
figure('Renderer', 'painters', 'Position', [10 10 900 600])
% figure(1)
% subplot(5,1,1)
% plot(1:size(n(1).values(1837:end),1),n(1).values(1837:end))
% title('Sterowanie');
% xlabel('Czas [s]'),ylabel('Wypełnienie'), grid on
% subplot(5,1,2)
% plot(1:size(n(1).values(1837:end),1),n(2).values(1837:end))
% title('Przemieszczenie');
% xlabel('Czas [s]'),ylabel('[m]'), grid on
% subplot(5,1,3)
% plot(1:size(n(1).values(1837:end),1),n(3).values(1837:end))
% title('Prędkość wózka');
% xlabel('Czas [s]'),ylabel('[m/s]'), grid on
% subplot(5,1,4)
plot((1:size(n(1).values(1837:end),1))/100,n(4).values(1837:end))
title('Kąt wahadła');
xlabel('Czas [s]'),ylabel('[rad]'), grid on
xlim([0 36])
% subplot(5,1,5)
% plot(1:size(n(1).values(1837:end),1),n(5).values(1837:end))
% title('Prędkość kątowa');
% xlabel('Czas [s]'),ylabel('[rad/s]'), grid on
saveas(gcf,'up_pend_plot','svg');