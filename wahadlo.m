%% Parametry wahadła

m = 0.2; % Masa wahadła
M = 0.5; % Masa wózka
I = 0.005426; % Bezwładność wahadła
L = 0.256; % Długość wahadła
l = 0.256;
f_p = 0.01; % Częstotliwość s
f_c = 0.01; % Częstotliwość c
g = 9.81; % Przyśpieszenie ziemskie
b = 0.1; % Współczynnik tarcia wózka

% A = [0, ((M+m)*g)/(M*L), b/(M*L), 0;
%      1,  0, 0, 0;
%      0, -(m*g)/M, -b/M, 1;
%      0, 0, 1, 0];
% 
% B = [1/(M*L);
%      0;
%      1/M;
%      0];
% 
% C = [1 0 0 0;
%      0 0 1 0];
% D = [0;
%      0];
p = I*(M+m)+M*m*l^2; %denominator for the A and B matrices

A = [0      1              0           0;
     0 -(I+m*l^2)*b/p  (m^2*g*l^2)/p   0;
     0      0              0           1;
     0 -(m*l*b)/p       m*g*l*(M+m)/p  0];
B = [     0;
     (I+m*l^2)/p;
          0;
        m*l/p];
C = [1 0 0 0;
     0 0 1 0];
D = 0;


sim('Schemat_wahadla.slx');

%% Pend_model

% Te nastawy wydają się odpowiadać układowi
m = 0.14; % Masa wahadła
F_t = 0.768;
M = 0.5; % Masa wózka
I = 0.00282; % Bezwładność wahadła
L = 0.36693; % Długość wahadła
l = L;
g = 9.81; % Przyśpieszenie ziemskie
b = 4; % Współczynnik tarcia wózka
b_c = 2;
b_p = 0.3;
cart_zad = 0;
theta_zad = 0;
distorption_delay = 0.5;
distorption_amplitude = pi/6;
% sim('pend_model.slx');
flag = 0;

%% Licczenie pierwiastków

 

M = 0.5;
m = 0.2;
l = 0.366;
I = 0.0055;
g = 9.81;
b = 0.1;
den = (I*(M+m) + M*m*l^2);

% Dla sinTheta = -Theta, cosTheta = -1, dTheta = 0, Theta = Pi:

A = [0.0, 1.0, 0.0, 0.0;
    (m*g*l*(M+m))/den, 0.0, 0.0, (-m*l*b)/den;
    0.0, 0.0, 0.0, 1.0;
    (m^2*g*l^2)/den, 0.0, 0.0, (-(I+m*l^2)*b)/den];

% Dla sinTheta = Theta, cosTheta = 1, dTheta = 0, Theta = 0:

% B = [0.0, 1.0, 0.0, 0.0;
%     (-m*g*l*(M+m))/den, 0.0, 0.0, (m*l*b)/den;
%     0.0, 0.0, 0.0, 1.0;
%     (m^2*g*l^2)/den, 0.0, 0.0, (-(I+m*l^2)*b)/den];

B = [0;
    -(m*l)/den;
    0;
    (I+m*l^2)/den];


Q = [1, 0 , 0, 0;
    0, 100, 0, 0;
    0, 0, 1, 0;
    0, 0, 0, 3];

% R = [0, 0, 0, 0;
%     0, 0.1, 0, 0;
%     0, 0, 0, 0;
%     0, 0, 0, 0.1];

R = 0.01;

K = lqr(A, B, Q, R);
K
% K = K';


%%

syms m a g theta(t)
eqn = m*a == -m*g*sin(theta)

syms r
eqn = subs(eqn,a,r*diff(theta,2))

eqn = isolate(eqn,diff(theta,2))

syms omega_0
eqn = subs(eqn,g/r,omega_0^2)

syms x
approx = taylor(sin(x),x,'Order',2);
approx = subs(approx,x,theta(t))

eqnLinear = subs(eqn,sin(theta(t)),approx)

syms theta_0 theta_t0
theta_t = diff(theta);
cond = [theta(0) == theta_0, theta_t(0) == theta_t0];
assume(omega_0,'real')
thetaSol(t) = dsolve(eqnLinear,cond)

gValue = 9.81;
rValue = 1;
omega_0Value = sqrt(gValue/rValue);
T = 2*pi/omega_0Value;

theta_0Value  = 0.1*pi; % Solution only valid for small angles.
theta_t0Value = 0;      % Initially at rest.

vars   = [omega_0      theta_0      theta_t0];
values = [omega_0Value theta_0Value theta_t0Value];
thetaSolPlot = subs(thetaSol,vars,values);

x_pos = sin(thetaSolPlot);
y_pos = -cos(thetaSolPlot);
fanimator(@fplot,x_pos,y_pos,'ko','MarkerFaceColor','k','AnimationRange',[0 5*T]);
hold on;
fanimator(@(t) plot([0 x_pos(t)],[0 y_pos(t)],'k-'),'AnimationRange',[0 5*T]);
fanimator(@(t) text(-0.3,0.3,"Timer: "+num2str(t,2)+" s"),'AnimationRange',[0 5*T]);

%%


