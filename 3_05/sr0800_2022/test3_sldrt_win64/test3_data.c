/*
 * test3_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test3".
 *
 * Model version              : 1.181
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed Apr 20 09:51:12 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "test3.h"
#include "test3_private.h"

/* Block parameters (auto storage) */
P_test3_T test3_P = {
  0.1,                                 /* Mask Parameter: PIDController1_D
                                        * Referenced by: '<S5>/Derivative Gain'
                                        */
  1.5,                                 /* Mask Parameter: PIDController_D
                                        * Referenced by: '<S4>/Derivative Gain'
                                        */
  0.0,                                 /* Mask Parameter: PIDController_I
                                        * Referenced by: '<S4>/Integral Gain'
                                        */
  0.0,                                 /* Mask Parameter: PIDController1_I
                                        * Referenced by: '<S5>/Integral Gain'
                                        */
  100.0,                               /* Mask Parameter: PIDController1_N
                                        * Referenced by: '<S5>/Filter Coefficient'
                                        */
  100.0,                               /* Mask Parameter: PIDController_N
                                        * Referenced by: '<S4>/Filter Coefficient'
                                        */
  5.0,                                 /* Mask Parameter: PIDController1_P
                                        * Referenced by: '<S5>/Proportional Gain'
                                        */
  0.01,                                /* Mask Parameter: Sensors_T0
                                        * Referenced by:
                                        *   '<S6>/Gain2'
                                        *   '<S6>/Gain4'
                                        */
  1.0,                                 /* Mask Parameter: AlfaNormalization_pos
                                        * Referenced by: '<S1>/pos'
                                        */
  0.9,                                 /* Expression: 0.9
                                        * Referenced by: '<Root>/Gain1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Normal'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Reset'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Constant2'
                                        */
  3.1415926535897931,                  /* Expression: pi
                                        * Referenced by: '<S1>/pi'
                                        */

  /*  Computed Parameter: Encoder_P1_Size
   * Referenced by: '<S2>/Encoder'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/Encoder'
                                        */

  /*  Computed Parameter: Encoder_P2_Size
   * Referenced by: '<S2>/Encoder'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/Encoder'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S2>/Encoder 500PPR'
                                        */
  0.0015339807878856412,               /* Expression: 2*pi/4096
                                        * Referenced by: '<S2>/Angle Scale'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S6>/Gain1'
                                        */
  20.0,                                /* Expression: 20
                                        * Referenced by: '<Root>/Gain_P '
                                        */
  -100.0,                              /* Computed Parameter: Transfer_D_A
                                        * Referenced by: '<Root>/Transfer_D '
                                        */
  -2000.0,                             /* Computed Parameter: Transfer_D_C
                                        * Referenced by: '<Root>/Transfer_D '
                                        */
  20.0,                                /* Computed Parameter: Transfer_D_D
                                        * Referenced by: '<Root>/Transfer_D '
                                        */
  9.0,                                 /* Expression: 9
                                        * Referenced by: '<Root>/Saturation'
                                        */
  -9.0,                                /* Expression: -9
                                        * Referenced by: '<Root>/Saturation'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<Root>/Gain'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Switch'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Normal1'
                                        */
  6.0501296456352633E-5,               /* Expression: 1.89/31239
                                        * Referenced by: '<S2>/PosCart Scale1'
                                        */
  -0.945,                              /* Expression: -1.89/2
                                        * Referenced by: '<S2>/Cart Offset'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S6>/Gain3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Constant3'
                                        */
  0.0,                                 /* Expression: InitialConditionForIntegrator
                                        * Referenced by: '<S5>/Integrator'
                                        */
  0.0,                                 /* Expression: InitialConditionForFilter
                                        * Referenced by: '<S5>/Filter'
                                        */

  /*  Computed Parameter: PWM_P1_Size
   * Referenced by: '<S2>/PWM'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/PWM'
                                        */

  /*  Computed Parameter: PWM_P2_Size
   * Referenced by: '<S2>/PWM'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/PWM'
                                        */
  0.5,                                 /* Expression: 0.5
                                        * Referenced by: '<S2>/Saturation'
                                        */
  -0.5,                                /* Expression: -0.5
                                        * Referenced by: '<S2>/Saturation'
                                        */

  /*  Computed Parameter: ResetEncoder_P1_Size
   * Referenced by: '<S2>/ResetEncoder'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ResetEncoder'
                                        */

  /*  Computed Parameter: ResetEncoder_P2_Size
   * Referenced by: '<S2>/ResetEncoder'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ResetEncoder'
                                        */

  /*  Expression: [0 0 0 ]
   * Referenced by: '<S2>/ResetSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Computed Parameter: LimitFlag_P1_Size
   * Referenced by: '<S2>/LimitFlag'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/LimitFlag'
                                        */

  /*  Computed Parameter: LimitFlag_P2_Size
   * Referenced by: '<S2>/LimitFlag'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/LimitFlag'
                                        */

  /*  Expression: [0 0 0]
   * Referenced by: '<S2>/LimitFlagSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [129 116 300]
   * Referenced by: '<S2>/LimitSource'
   */
  { 129.0, 116.0, 300.0 },

  /*  Computed Parameter: SetLimit_P1_Size
   * Referenced by: '<S2>/SetLimit'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/SetLimit'
                                        */

  /*  Computed Parameter: SetLimit_P2_Size
   * Referenced by: '<S2>/SetLimit'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/SetLimit'
                                        */

  /*  Computed Parameter: LimitSwitch_P1_Size
   * Referenced by: '<S2>/LimitSwitch'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/LimitSwitch'
                                        */

  /*  Computed Parameter: LimitSwitch_P2_Size
   * Referenced by: '<S2>/LimitSwitch'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/LimitSwitch'
                                        */

  /*  Computed Parameter: PWMPrescaler_P1_Size
   * Referenced by: '<S2>/PWMPrescaler'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/PWMPrescaler'
                                        */

  /*  Computed Parameter: PWMPrescaler_P2_Size
   * Referenced by: '<S2>/PWMPrescaler'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/PWMPrescaler'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S2>/PWMPrescalerSource'
                                        */

  /*  Computed Parameter: ResetSwitchFlag_P1_Size
   * Referenced by: '<S2>/ResetSwitchFlag '
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ResetSwitchFlag '
                                        */

  /*  Computed Parameter: ResetSwitchFlag_P2_Size
   * Referenced by: '<S2>/ResetSwitchFlag '
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ResetSwitchFlag '
                                        */

  /*  Expression: [ 0 0 0 ]
   * Referenced by: '<S2>/ResetSwitchFlagSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Computed Parameter: ThermFlag_P1_Size
   * Referenced by: '<S2>/ThermFlag '
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ThermFlag '
                                        */

  /*  Computed Parameter: ThermFlag_P2_Size
   * Referenced by: '<S2>/ThermFlag '
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ThermFlag '
                                        */

  /*  Expression: [1 1 1]
   * Referenced by: '<S2>/ThermFlagSource'
   */
  { 1.0, 1.0, 1.0 },
  0.0,                                 /* Expression: InitialConditionForFilter
                                        * Referenced by: '<S4>/Filter'
                                        */
  0.0,                                 /* Expression: InitialConditionForIntegrator
                                        * Referenced by: '<S4>/Integrator'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory4'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory5'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory6'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory7'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory8'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Memory9'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Data Store Memory'
                                        */
  1U,                                  /* Computed Parameter: TurnOffControl_CurrentSetting
                                        * Referenced by: '<Root>/Turn Off Control'
                                        */
  0U                                   /* Computed Parameter: ResetEncoders_CurrentSetting
                                        * Referenced by: '<Root>/Reset Encoders'
                                        */
};
