  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 2;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test1_P)
    ;%
      section.nData     = 92;
      section.data(92)  = dumData; %prealloc
      
	  ;% test1_P.PIDController1_D
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test1_P.PIDController_D
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test1_P.PIDController_I
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% test1_P.PIDController1_I
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% test1_P.PIDController1_N
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% test1_P.PIDController_N
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% test1_P.PIDController1_P
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% test1_P.Sensors_T0
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% test1_P.AlfaNormalization_pos
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% test1_P.Normal_Value
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% test1_P.Reset_Value
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% test1_P.Constant2_Value
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% test1_P.pi_Value
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% test1_P.Encoder_P1_Size
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% test1_P.Encoder_P1
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 15;
	
	  ;% test1_P.Encoder_P2_Size
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 16;
	
	  ;% test1_P.Encoder_P2
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 18;
	
	  ;% test1_P.Encoder500PPR_Gain
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 19;
	
	  ;% test1_P.AngleScale_Gain
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 20;
	
	  ;% test1_P.Gain1_Gain
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 21;
	
	  ;% test1_P.Constant1_Value
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 22;
	
	  ;% test1_P.Integrator_IC
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 23;
	
	  ;% test1_P.GainN_Gain
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 24;
	
	  ;% test1_P.P_Gain
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 25;
	
	  ;% test1_P.Constant4_Value
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 26;
	
	  ;% test1_P.Integrator1_IC
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 27;
	
	  ;% test1_P.GainN1_Gain
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 28;
	
	  ;% test1_P.ConstantN_Value
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 29;
	
	  ;% test1_P.D_Gain
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 30;
	
	  ;% test1_P.Saturation_UpperSat
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 31;
	
	  ;% test1_P.Saturation_LowerSat
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 32;
	
	  ;% test1_P.Gain_Gain
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 33;
	
	  ;% test1_P.Normal1_Value
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 34;
	
	  ;% test1_P.PosCartScale1_Gain
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 35;
	
	  ;% test1_P.CartOffset_Value
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 36;
	
	  ;% test1_P.Gain3_Gain
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 37;
	
	  ;% test1_P.Memory1_X0
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 38;
	
	  ;% test1_P.Memory_X0
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 39;
	
	  ;% test1_P.Constant3_Value
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 40;
	
	  ;% test1_P.Integrator_IC_j
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 41;
	
	  ;% test1_P.Filter_IC
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 42;
	
	  ;% test1_P.PWM_P1_Size
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 43;
	
	  ;% test1_P.PWM_P1
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 45;
	
	  ;% test1_P.PWM_P2_Size
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 46;
	
	  ;% test1_P.PWM_P2
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 48;
	
	  ;% test1_P.Saturation_UpperSat_c
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 49;
	
	  ;% test1_P.Saturation_LowerSat_l
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 50;
	
	  ;% test1_P.ResetEncoder_P1_Size
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 51;
	
	  ;% test1_P.ResetEncoder_P1
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 53;
	
	  ;% test1_P.ResetEncoder_P2_Size
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 54;
	
	  ;% test1_P.ResetEncoder_P2
	  section.data(51).logicalSrcIdx = 50;
	  section.data(51).dtTransOffset = 56;
	
	  ;% test1_P.ResetSource_Value
	  section.data(52).logicalSrcIdx = 51;
	  section.data(52).dtTransOffset = 57;
	
	  ;% test1_P.LimitFlag_P1_Size
	  section.data(53).logicalSrcIdx = 52;
	  section.data(53).dtTransOffset = 60;
	
	  ;% test1_P.LimitFlag_P1
	  section.data(54).logicalSrcIdx = 53;
	  section.data(54).dtTransOffset = 62;
	
	  ;% test1_P.LimitFlag_P2_Size
	  section.data(55).logicalSrcIdx = 54;
	  section.data(55).dtTransOffset = 63;
	
	  ;% test1_P.LimitFlag_P2
	  section.data(56).logicalSrcIdx = 55;
	  section.data(56).dtTransOffset = 65;
	
	  ;% test1_P.LimitFlagSource_Value
	  section.data(57).logicalSrcIdx = 56;
	  section.data(57).dtTransOffset = 66;
	
	  ;% test1_P.LimitSource_Value
	  section.data(58).logicalSrcIdx = 57;
	  section.data(58).dtTransOffset = 69;
	
	  ;% test1_P.SetLimit_P1_Size
	  section.data(59).logicalSrcIdx = 58;
	  section.data(59).dtTransOffset = 72;
	
	  ;% test1_P.SetLimit_P1
	  section.data(60).logicalSrcIdx = 59;
	  section.data(60).dtTransOffset = 74;
	
	  ;% test1_P.SetLimit_P2_Size
	  section.data(61).logicalSrcIdx = 60;
	  section.data(61).dtTransOffset = 75;
	
	  ;% test1_P.SetLimit_P2
	  section.data(62).logicalSrcIdx = 61;
	  section.data(62).dtTransOffset = 77;
	
	  ;% test1_P.LimitSwitch_P1_Size
	  section.data(63).logicalSrcIdx = 62;
	  section.data(63).dtTransOffset = 78;
	
	  ;% test1_P.LimitSwitch_P1
	  section.data(64).logicalSrcIdx = 63;
	  section.data(64).dtTransOffset = 80;
	
	  ;% test1_P.LimitSwitch_P2_Size
	  section.data(65).logicalSrcIdx = 64;
	  section.data(65).dtTransOffset = 81;
	
	  ;% test1_P.LimitSwitch_P2
	  section.data(66).logicalSrcIdx = 65;
	  section.data(66).dtTransOffset = 83;
	
	  ;% test1_P.PWMPrescaler_P1_Size
	  section.data(67).logicalSrcIdx = 66;
	  section.data(67).dtTransOffset = 84;
	
	  ;% test1_P.PWMPrescaler_P1
	  section.data(68).logicalSrcIdx = 67;
	  section.data(68).dtTransOffset = 86;
	
	  ;% test1_P.PWMPrescaler_P2_Size
	  section.data(69).logicalSrcIdx = 68;
	  section.data(69).dtTransOffset = 87;
	
	  ;% test1_P.PWMPrescaler_P2
	  section.data(70).logicalSrcIdx = 69;
	  section.data(70).dtTransOffset = 89;
	
	  ;% test1_P.PWMPrescalerSource_Value
	  section.data(71).logicalSrcIdx = 70;
	  section.data(71).dtTransOffset = 90;
	
	  ;% test1_P.ResetSwitchFlag_P1_Size
	  section.data(72).logicalSrcIdx = 71;
	  section.data(72).dtTransOffset = 91;
	
	  ;% test1_P.ResetSwitchFlag_P1
	  section.data(73).logicalSrcIdx = 72;
	  section.data(73).dtTransOffset = 93;
	
	  ;% test1_P.ResetSwitchFlag_P2_Size
	  section.data(74).logicalSrcIdx = 73;
	  section.data(74).dtTransOffset = 94;
	
	  ;% test1_P.ResetSwitchFlag_P2
	  section.data(75).logicalSrcIdx = 74;
	  section.data(75).dtTransOffset = 96;
	
	  ;% test1_P.ResetSwitchFlagSource_Value
	  section.data(76).logicalSrcIdx = 75;
	  section.data(76).dtTransOffset = 97;
	
	  ;% test1_P.ThermFlag_P1_Size
	  section.data(77).logicalSrcIdx = 76;
	  section.data(77).dtTransOffset = 100;
	
	  ;% test1_P.ThermFlag_P1
	  section.data(78).logicalSrcIdx = 77;
	  section.data(78).dtTransOffset = 102;
	
	  ;% test1_P.ThermFlag_P2_Size
	  section.data(79).logicalSrcIdx = 78;
	  section.data(79).dtTransOffset = 103;
	
	  ;% test1_P.ThermFlag_P2
	  section.data(80).logicalSrcIdx = 79;
	  section.data(80).dtTransOffset = 105;
	
	  ;% test1_P.ThermFlagSource_Value
	  section.data(81).logicalSrcIdx = 80;
	  section.data(81).dtTransOffset = 106;
	
	  ;% test1_P.Filter_IC_n
	  section.data(82).logicalSrcIdx = 81;
	  section.data(82).dtTransOffset = 109;
	
	  ;% test1_P.Integrator_IC_g
	  section.data(83).logicalSrcIdx = 82;
	  section.data(83).dtTransOffset = 110;
	
	  ;% test1_P.Memory2_X0
	  section.data(84).logicalSrcIdx = 83;
	  section.data(84).dtTransOffset = 111;
	
	  ;% test1_P.Memory3_X0
	  section.data(85).logicalSrcIdx = 84;
	  section.data(85).dtTransOffset = 112;
	
	  ;% test1_P.Memory4_X0
	  section.data(86).logicalSrcIdx = 85;
	  section.data(86).dtTransOffset = 113;
	
	  ;% test1_P.Memory5_X0
	  section.data(87).logicalSrcIdx = 86;
	  section.data(87).dtTransOffset = 114;
	
	  ;% test1_P.Memory6_X0
	  section.data(88).logicalSrcIdx = 87;
	  section.data(88).dtTransOffset = 115;
	
	  ;% test1_P.Memory7_X0
	  section.data(89).logicalSrcIdx = 88;
	  section.data(89).dtTransOffset = 116;
	
	  ;% test1_P.Memory8_X0
	  section.data(90).logicalSrcIdx = 89;
	  section.data(90).dtTransOffset = 117;
	
	  ;% test1_P.Memory9_X0
	  section.data(91).logicalSrcIdx = 90;
	  section.data(91).dtTransOffset = 118;
	
	  ;% test1_P.DataStoreMemory_InitialValue
	  section.data(92).logicalSrcIdx = 91;
	  section.data(92).dtTransOffset = 119;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% test1_P.TurnOffControl_CurrentSetting
	  section.data(1).logicalSrcIdx = 92;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test1_P.ResetEncoders_CurrentSetting
	  section.data(2).logicalSrcIdx = 93;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test1_B)
    ;%
      section.nData     = 47;
      section.data(47)  = dumData; %prealloc
      
	  ;% test1_B.Encoder
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test1_B.AngleScale
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 5;
	
	  ;% test1_B.PendPosOut
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 6;
	
	  ;% test1_B.Sum
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 7;
	
	  ;% test1_B.Product1
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 8;
	
	  ;% test1_B.D
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 9;
	
	  ;% test1_B.Product2
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 10;
	
	  ;% test1_B.Add
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 11;
	
	  ;% test1_B.Gain
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 12;
	
	  ;% test1_B.Control
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 13;
	
	  ;% test1_B.Sum_p
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 14;
	
	  ;% test1_B.Cart_pos
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 15;
	
	  ;% test1_B.Cart_vel
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 16;
	
	  ;% test1_B.Pend_vel
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 17;
	
	  ;% test1_B.Sum1
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 18;
	
	  ;% test1_B.ProportionalGain
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 19;
	
	  ;% test1_B.DerivativeGain
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 20;
	
	  ;% test1_B.FilterCoefficient
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 21;
	
	  ;% test1_B.Sum_m
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 22;
	
	  ;% test1_B.PWM
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 23;
	
	  ;% test1_B.Saturation
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 26;
	
	  ;% test1_B.ResetEncoder
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 29;
	
	  ;% test1_B.ResetSource
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 34;
	
	  ;% test1_B.LimitFlag
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 37;
	
	  ;% test1_B.LimitFlagSource
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 40;
	
	  ;% test1_B.LimitSource
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 43;
	
	  ;% test1_B.SetLimit
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 46;
	
	  ;% test1_B.LimitSwitch
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 49;
	
	  ;% test1_B.PWMPrescaler
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 52;
	
	  ;% test1_B.PWMPrescalerSource
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 53;
	
	  ;% test1_B.ResetSwitchFlag
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 54;
	
	  ;% test1_B.ResetSwitchFlagSource
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 57;
	
	  ;% test1_B.ThermFlag
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 60;
	
	  ;% test1_B.ThermFlagSource
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 63;
	
	  ;% test1_B.DerivativeGain_p
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 66;
	
	  ;% test1_B.FilterCoefficient_b
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 67;
	
	  ;% test1_B.IntegralGain
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 68;
	
	  ;% test1_B.IntegralGain_o
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 69;
	
	  ;% test1_B.ResetEncoders
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 70;
	
	  ;% test1_B.Memory2
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 71;
	
	  ;% test1_B.Memory3
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 72;
	
	  ;% test1_B.Memory4
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 73;
	
	  ;% test1_B.Memory5
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 74;
	
	  ;% test1_B.Memory6
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 75;
	
	  ;% test1_B.Memory7
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 76;
	
	  ;% test1_B.Memory8
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 77;
	
	  ;% test1_B.Memory9
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 78;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 2;
    sectIdxOffset = 1;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (test1_DW)
    ;%
      section.nData     = 11;
      section.data(11)  = dumData; %prealloc
      
	  ;% test1_DW.Memory1_PreviousInput
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test1_DW.Memory_PreviousInput
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test1_DW.Memory2_PreviousInput
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% test1_DW.Memory3_PreviousInput
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% test1_DW.Memory4_PreviousInput
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% test1_DW.Memory5_PreviousInput
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% test1_DW.Memory6_PreviousInput
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% test1_DW.Memory7_PreviousInput
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% test1_DW.Memory8_PreviousInput
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% test1_DW.Memory9_PreviousInput
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% test1_DW.flag
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% test1_DW.PendulumControlandStatesExperiment_PWORK.LoggedData
	  section.data(1).logicalSrcIdx = 11;
	  section.data(1).dtTransOffset = 0;
	
	  ;% test1_DW.Scope_PWORK.LoggedData
	  section.data(2).logicalSrcIdx = 12;
	  section.data(2).dtTransOffset = 1;
	
	  ;% test1_DW.Scope1_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 13;
	  section.data(3).dtTransOffset = 2;
	
	  ;% test1_DW.Scope2_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 14;
	  section.data(4).dtTransOffset = 3;
	
	  ;% test1_DW.Scope3_PWORK.LoggedData
	  section.data(5).logicalSrcIdx = 15;
	  section.data(5).dtTransOffset = 4;
	
	  ;% test1_DW.Scope4_PWORK.LoggedData
	  section.data(6).logicalSrcIdx = 16;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 339807993;
  targMap.checksum1 = 427003870;
  targMap.checksum2 = 1105148219;
  targMap.checksum3 = 1284371365;

