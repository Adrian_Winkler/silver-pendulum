/*
 * test4_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "test4".
 *
 * Model version              : 1.182
 * Simulink Coder version : 8.8.1 (R2015aSP1) 04-Sep-2015
 * C source code generated on : Wed Apr 20 10:15:30 2022
 *
 * Target selection: rtwin.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "test4.h"
#include "test4_private.h"

/* Block parameters (auto storage) */
P_test4_T test4_P = {
  0.0055,                              /* Variable: I
                                        * Referenced by:
                                        *   '<S6>/Constant'
                                        *   '<S6>/Gain3'
                                        */

  /*  Variable: K
   * Referenced by: '<Root>/Gain2'
   */
  { -89.363466628881724, -28.122551167247288, -1.0000000000001201,
    -1.8912442475388618 },
  0.36693,                             /* Variable: L
                                        * Referenced by:
                                        *   '<S6>/Gain'
                                        *   '<S6>/Gain1'
                                        *   '<S6>/Gain5'
                                        *   '<S6>/Gain6'
                                        */
  0.5,                                 /* Variable: M
                                        * Referenced by:
                                        *   '<S6>/Constant'
                                        *   '<S6>/Gain6'
                                        */
  2.0,                                 /* Variable: b_c
                                        * Referenced by: '<S6>/Gain2'
                                        */
  0.3,                                 /* Variable: b_p
                                        * Referenced by: '<S6>/Gain5'
                                        */
  9.81,                                /* Variable: g
                                        * Referenced by:
                                        *   '<S6>/Constant1'
                                        *   '<S6>/Gain6'
                                        */
  0.2,                                 /* Variable: m
                                        * Referenced by:
                                        *   '<S6>/Constant'
                                        *   '<S6>/Gain'
                                        *   '<S6>/Gain1'
                                        *   '<S6>/Gain3'
                                        *   '<S6>/Gain5'
                                        *   '<S6>/Gain6'
                                        */
  0.01,                                /* Mask Parameter: Sensors_T0
                                        * Referenced by:
                                        *   '<S5>/Gain2'
                                        *   '<S5>/Gain4'
                                        */
  1.0,                                 /* Mask Parameter: AlfaNormalization_pos
                                        * Referenced by: '<S1>/pos'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Normal2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Normal1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Gain4'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Gain3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Switch'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Normal'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Reset'
                                        */
  3.1415926535897931,                  /* Expression: pi
                                        * Referenced by: '<S1>/pi'
                                        */

  /*  Computed Parameter: Encoder_P1_Size
   * Referenced by: '<S2>/Encoder'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/Encoder'
                                        */

  /*  Computed Parameter: Encoder_P2_Size
   * Referenced by: '<S2>/Encoder'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/Encoder'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S2>/Encoder 500PPR'
                                        */
  0.0015339807878856412,               /* Expression: 2*pi/4096
                                        * Referenced by: '<S2>/Angle Scale'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S5>/Gain1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory'
                                        */
  6.0501296456352633E-5,               /* Expression: 1.89/31239
                                        * Referenced by: '<S2>/PosCart Scale1'
                                        */
  -0.945,                              /* Expression: -1.89/2
                                        * Referenced by: '<S2>/Cart Offset'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S5>/Gain3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory1'
                                        */
  9.0,                                 /* Expression: 9
                                        * Referenced by: '<Root>/Saturation'
                                        */
  -9.0,                                /* Expression: -9
                                        * Referenced by: '<Root>/Saturation'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Integrator3'
                                        */
  0.000461,                            /* Expression: 0.000461
                                        * Referenced by: '<S6>/Quantizer1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Integrator1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Constant'
                                        */
  0.00153435,                          /* Expression: 0.00153435
                                        * Referenced by: '<S6>/Quantizer'
                                        */

  /*  Expression: pInitialization.M
   * Referenced by: '<S7>/KalmanGainM'
   */
  { 0.99898044772067063, -0.019326228988454965, 0.000517163790913853,
    -0.00837469794830578, 0.00051716379091396784, -0.21521369624036341,
    0.9942372828002527, -0.094563249495668716 },

  /*  Expression: pInitialization.C
   * Referenced by: '<S3>/C'
   */
  { 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0 },

  /*  Expression: pInitialization.D
   * Referenced by: '<S3>/D'
   */
  { 0.0, 0.0 },

  /*  Expression: pInitialization.X0
   * Referenced by: '<S3>/X0'
   */
  { 0.0, 0.0, 0.0, 0.0 },

  /*  Computed Parameter: PWM_P1_Size
   * Referenced by: '<S2>/PWM'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/PWM'
                                        */

  /*  Computed Parameter: PWM_P2_Size
   * Referenced by: '<S2>/PWM'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/PWM'
                                        */
  0.5,                                 /* Expression: 0.5
                                        * Referenced by: '<S2>/Saturation'
                                        */
  -0.5,                                /* Expression: -0.5
                                        * Referenced by: '<S2>/Saturation'
                                        */

  /*  Computed Parameter: ResetEncoder_P1_Size
   * Referenced by: '<S2>/ResetEncoder'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ResetEncoder'
                                        */

  /*  Computed Parameter: ResetEncoder_P2_Size
   * Referenced by: '<S2>/ResetEncoder'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ResetEncoder'
                                        */

  /*  Expression: [0 0 0 ]
   * Referenced by: '<S2>/ResetSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Computed Parameter: LimitFlag_P1_Size
   * Referenced by: '<S2>/LimitFlag'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/LimitFlag'
                                        */

  /*  Computed Parameter: LimitFlag_P2_Size
   * Referenced by: '<S2>/LimitFlag'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/LimitFlag'
                                        */

  /*  Expression: [0 0 0]
   * Referenced by: '<S2>/LimitFlagSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [129 116 300]
   * Referenced by: '<S2>/LimitSource'
   */
  { 129.0, 116.0, 300.0 },

  /*  Computed Parameter: SetLimit_P1_Size
   * Referenced by: '<S2>/SetLimit'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/SetLimit'
                                        */

  /*  Computed Parameter: SetLimit_P2_Size
   * Referenced by: '<S2>/SetLimit'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/SetLimit'
                                        */

  /*  Computed Parameter: LimitSwitch_P1_Size
   * Referenced by: '<S2>/LimitSwitch'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/LimitSwitch'
                                        */

  /*  Computed Parameter: LimitSwitch_P2_Size
   * Referenced by: '<S2>/LimitSwitch'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/LimitSwitch'
                                        */

  /*  Computed Parameter: PWMPrescaler_P1_Size
   * Referenced by: '<S2>/PWMPrescaler'
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/PWMPrescaler'
                                        */

  /*  Computed Parameter: PWMPrescaler_P2_Size
   * Referenced by: '<S2>/PWMPrescaler'
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/PWMPrescaler'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S2>/PWMPrescalerSource'
                                        */

  /*  Computed Parameter: ResetSwitchFlag_P1_Size
   * Referenced by: '<S2>/ResetSwitchFlag '
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ResetSwitchFlag '
                                        */

  /*  Computed Parameter: ResetSwitchFlag_P2_Size
   * Referenced by: '<S2>/ResetSwitchFlag '
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ResetSwitchFlag '
                                        */

  /*  Expression: [ 0 0 0 ]
   * Referenced by: '<S2>/ResetSwitchFlagSource'
   */
  { 0.0, 0.0, 0.0 },

  /*  Computed Parameter: ThermFlag_P1_Size
   * Referenced by: '<S2>/ThermFlag '
   */
  { 1.0, 1.0 },
  57088.0,                             /* Expression: BaseAddress
                                        * Referenced by: '<S2>/ThermFlag '
                                        */

  /*  Computed Parameter: ThermFlag_P2_Size
   * Referenced by: '<S2>/ThermFlag '
   */
  { 1.0, 1.0 },
  0.01,                                /* Expression: T0
                                        * Referenced by: '<S2>/ThermFlag '
                                        */

  /*  Expression: [1 1 1]
   * Referenced by: '<S2>/ThermFlagSource'
   */
  { 1.0, 1.0, 1.0 },
  -0.8,                                /* Expression: -0.8
                                        * Referenced by: '<Root>/Gain1'
                                        */

  /*  Expression: pInitialization.A
   * Referenced by: '<S3>/A'
   */
  { 0.0, 29.14739991650044, 0.0, 3.0479852484111896, 1.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, -0.42445609314839727, 1.0, -0.18724312288351813 },

  /*  Expression: pInitialization.B
   * Referenced by: '<S3>/B'
   */
  { 0.0, -4.2445609314839725, 0.0, 1.8724312288351812 },

  /*  Expression: pInitialization.L
   * Referenced by: '<S7>/KalmanGainL'
   */
  { -0.019326228988454965, 29.121237310051484, -0.00837469794830578,
    3.0464457727008569, -0.21521369624036341, 0.055211927272450181,
    -0.094563249495668716, 0.019282625751300367 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory4'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory5'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory6'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory7'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory8'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Memory9'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Integrator2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/Integrator'
                                        */
  0.005385504996,                      /* Expression: (m^2)*(L^2)
                                        * Referenced by: '<S6>/Gain7'
                                        */
  0.005385504996,                      /* Expression: (m^2)*(L^2)
                                        * Referenced by: '<S6>/Gain4'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Data Store Memory'
                                        */
  1U,                                  /* Computed Parameter: MemoryX_DelayLength
                                        * Referenced by: '<S3>/MemoryX'
                                        */
  0U,                                  /* Computed Parameter: TurnOffControl_CurrentSetting
                                        * Referenced by: '<Root>/Turn Off Control'
                                        */
  0U,                                  /* Computed Parameter: TurnOffControl1_CurrentSetting
                                        * Referenced by: '<Root>/Turn Off Control1'
                                        */
  0U,                                  /* Computed Parameter: ResetEncoders_CurrentSetting
                                        * Referenced by: '<Root>/Reset Encoders'
                                        */
  1                                    /* Computed Parameter: Enable_Value
                                        * Referenced by: '<S3>/Enable'
                                        */
};
